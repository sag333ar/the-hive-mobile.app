import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:the_hive_app/screens/hive_post/hive_post_screen.dart';
import 'package:the_hive_app/screens/home_page.dart';
import 'package:the_hive_app/screens/login/hive_auth_login_screen.dart';
import 'package:the_hive_app/screens/user_channel/user_channel_container.dart';
import 'package:the_hive_app/utilities/hive_bloc.dart';
import 'package:the_hive_app/utilities/request_generator.dart';

class HiveApp extends StatelessWidget {
  const HiveApp({Key? key}) : super(key: key);

  ThemeData getTheme(Color color, bool isDark) {
    return (isDark ? ThemeData.dark() : ThemeData.light()).copyWith(
      colorScheme: ColorScheme.fromSeed(seedColor: color),
      primaryColor: color,
      primaryColorDark: color,
      primaryColorLight: color,
      appBarTheme: AppBarTheme(
        color: color,
      ),
      floatingActionButtonTheme: FloatingActionButtonThemeData(
        backgroundColor: color,
      ),
    );
  }

  MaterialPageRoute? _getRouteForPostDetails(
    RouteSettings settings,
    HiveBlocData data,
  ) {
    var name = settings.name ?? "";
    RegExp regExp = RegExp(r'/@(.+)/(.+)');
    RegExpMatch? match = regExp.firstMatch(name);
    var matchAuthor = match?.group(1);
    var matchPermlink = match?.group(2);
    if (matchAuthor != null && matchPermlink != null) {
      return MaterialPageRoute(
        builder: (c) => HivePostScreen(
          author: matchAuthor,
          permlink: matchPermlink,
          appData: data,
        ),
        settings: settings,
      );
    }
    return null;
  }

  MaterialPageRoute? _getRouteForUserChannel(
    RouteSettings settings,
    HiveBlocData data,
  ) {
    var name = settings.name ?? "";
    RegExp regExp = RegExp(r'/@(.+)');
    RegExpMatch? match = regExp.firstMatch(name);
    var matchAuthor = match?.group(1);
    if (matchAuthor != null) {
      return MaterialPageRoute(
        builder: (c) =>
            UserChannelContainerScreen(username: matchAuthor, appData: data),
        settings: settings,
      );
    }
    return null;
  }

  Route<dynamic> onGenRoute(RouteSettings settings, HiveBlocData data) {
    var name = settings.name;
    if (name == null) {
      return MaterialPageRoute(
        builder: (c) => const MyHomePage(
          feed: HomeFeedStyle.trending,
          tag: null,
          communityName: null,
        ),
        settings: settings,
      );
    } else if (name == "/" || name == "/trending") {
      return MaterialPageRoute(
        builder: (c) => const MyHomePage(
          feed: HomeFeedStyle.trending,
          tag: null,
          communityName: null,
        ),
        settings: settings,
      );
    } else if (name == "/hot") {
      return MaterialPageRoute(
        builder: (c) => const MyHomePage(
          feed: HomeFeedStyle.hot,
          tag: null,
          communityName: null,
        ),
        settings: settings,
      );
    } else if (name == "/created") {
      return MaterialPageRoute(
        builder: (c) => const MyHomePage(
          feed: HomeFeedStyle.newFeed,
          tag: null,
          communityName: null,
        ),
        settings: settings,
      );
    } else if (name == '/login') {
      // HiveAuthBasedLoginScreen
      return MaterialPageRoute(
        builder: (c) => HiveAuthBasedLoginScreen(data: data),
        settings: settings,
      );
    } else {
      var postDetailsRoute = _getRouteForPostDetails(settings, data);
      if (postDetailsRoute != null) {
        return postDetailsRoute;
      }
      var userRoute = _getRouteForUserChannel(settings, data);
      if (userRoute != null) {
        return userRoute;
      }
      return MaterialPageRoute(
        builder: (c) => const MyHomePage(
          feed: HomeFeedStyle.trending,
          tag: null,
          communityName: null,
        ),
        settings: settings,
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    var data = Provider.of<HiveBlocData>(context);
    return MaterialApp(
      title: 'The-Hive-Mobile.app',
      debugShowCheckedModeBanner: false,
      theme: data.isDarkMode
          ? getTheme(hiveBloc.themeColor, true)
          : getTheme(hiveBloc.themeColor, false),
      initialRoute: '/',
      onGenerateRoute: (s) => onGenRoute(s, data),
    );
  }
}
