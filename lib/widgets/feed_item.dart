import 'package:flutter/material.dart';
import 'package:reading_time/reading_time.dart';
import 'package:the_hive_app/utilities/hive_bloc.dart';
import 'package:the_hive_app/utilities/models/feed.dart';
import 'package:the_hive_app/widgets/custom_circle_avatar.dart';
import 'package:timeago/timeago.dart' as timeago;

class FeedItemWidget extends StatelessWidget {
  const FeedItemWidget({
    Key? key,
    required this.item,
    required this.onUserProfileTapped,
    required this.onPostTapped,
  }) : super(key: key);
  final HivePostJsonItem item;
  final Function onUserProfileTapped;
  final Function onPostTapped;

  Widget _errorIndicator(double width) {
    return Container(
      height: 160,
      decoration: BoxDecoration(
        image: DecorationImage(
          image: Image.asset('assets/Icon-maskable-512.png').image,
          fit: BoxFit.fitHeight,
        ),
      ),
    );
  }

  Widget _imageThumb(String url, double width) {
    return FadeInImage.assetNetwork(
      height: 160,
      fit: BoxFit.cover,
      placeholder: 'assets/Icon-maskable-512.png',
      image: hiveBloc.resizedImage(url),
      placeholderErrorBuilder:
          (BuildContext context, Object error, StackTrace? stackTrace) {
        return _errorIndicator(width);
      },
      imageErrorBuilder:
          (BuildContext context, Object error, StackTrace? stackTrace) {
        return _errorIndicator(width);
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width;
    var timeAgo =
        timeago.format(DateTime.tryParse(item.created ?? '') ?? DateTime.now());
    String readTime = readingTime(item.body ?? '').msg;
    readTime = readTime == "less than a minute" ? "1 min read" : readTime;
    return SizedBox(
      child: Card(
        child: Column(
          children: [
            InkWell(
              child: ListTile(
                leading: CustomCircleAvatar(
                  url: hiveBloc.userOwnerThumb(item.author ?? ''),
                  width: 44,
                  height: 44,
                ),
                title: Text('@${item.author ?? 'no-name'}'),
                trailing: item.percentHbd == 0
                    ? Image.asset(
                        'assets/Icon-maskable-512.png',
                        height: 20,
                        width: 20,
                      )
                    : null,
              ),
              onTap: () {
                onUserProfileTapped();
              },
            ),
            InkWell(
              child: Column(
                children: [
                  ListTile(
                    title: item.jsonMetadata?.image?.isNotEmpty == true
                        ? _imageThumb(item.jsonMetadata!.image!.first, width)
                        : _errorIndicator(width),
                  ),
                  ListTile(
                    title: Text(
                      item.title,
                      maxLines: 2,
                    ),
                    subtitle: Text(
                      '\$ ${(item.payout ?? 0.0).toStringAsFixed(3)} | 👍 ${item.activeVotes?.length ?? 0} | 💬 ${item.children ?? 0} | ⏱️ $timeAgo\n${item.communityTitle?.isEmpty == true ? '' : '🔖 in ${item.communityTitle ?? ''} · '}👁️ $readTime',
                      // textAlign: TextAlign.center,
                    ),
                  ),
                ],
              ),
              onTap: () {
                onPostTapped();
              },
            )
          ],
        ),
      ),
    );
  }
}
