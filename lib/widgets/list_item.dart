import 'package:flutter/material.dart';
import 'package:markdown/markdown.dart' as md;
import 'package:reading_time/reading_time.dart';
import 'package:the_hive_app/utilities/hive_bloc.dart';
import 'package:the_hive_app/utilities/models/feed.dart';
import 'package:the_hive_app/widgets/custom_circle_avatar.dart';
import 'package:timeago/timeago.dart' as timeago;

class ListItemWidget extends StatelessWidget {
  const ListItemWidget({
    Key? key,
    required this.item,
    required this.onUserProfileTapped,
    required this.onPostTapped,
    required this.isUserChannel,
    required this.shouldShowPostImage,
  }) : super(key: key);
  final HivePostJsonItem item;
  final Function onUserProfileTapped;
  final Function onPostTapped;
  final bool isUserChannel;
  final bool shouldShowPostImage;

  Widget _headerRow() {
    var categoryOrTag = item.communityTitle?.isEmpty == true
        ? "# ${item.jsonMetadata?.tags?.first ?? "Hive"}"
        : "🔖 ${item.communityTitle!}";
    return Row(
      children: [
        InkWell(
          onTap: () {
            onUserProfileTapped();
          },
          child: Row(
            children: [
              CustomCircleAvatar(
                url: hiveBloc.userOwnerThumb(item.author ?? ''),
                width: 30,
                height: 30,
              ),
              const SizedBox(width: 5),
              Text(
                item.author ?? 'No Author',
                style: const TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 16,
                ),
              ),
            ],
          ),
        ),
        const SizedBox(width: 5),
        Icon(Icons.fence, size: 8, color: hiveBloc.themeColor),
        const SizedBox(width: 5),
        Text(categoryOrTag),
      ],
    );
  }

  Widget _leftImage() {
    var imageUrl = item.jsonMetadata != null
        ? item.jsonMetadata!.image != null &&
                item.jsonMetadata!.image!.isNotEmpty
            ? item.jsonMetadata!.image!.first
            : "https://cryptologos.cc/logos/hive-blockchain-hive-logo.png?v=024"
        : "https://cryptologos.cc/logos/hive-blockchain-hive-logo.png?v=024";
    return Container(
      width: 140,
      height: 93.33,
      decoration: BoxDecoration(
        color: hiveBloc.themeColor.withOpacity(0.2),
        borderRadius: BorderRadius.circular(10),
        image: DecorationImage(
          image: Image.network(
            imageUrl,
            errorBuilder: (context, object, stack) {
              return Image.asset('assets/Icon-maskable-512.png');
            },
            fit: BoxFit.fitWidth,
          ).image,
        ),
      ),
    );
  }

  Widget _body(double width) {
    var body = md.markdownToHtml(item.body ?? "No text content");
    var regExp = RegExp("(<[^>]+>|\r|\n)");
    body = body.replaceAllMapped(regExp, (match) => "");
    var regExpUrl = RegExp("https?:\/\/([a-zA-Z0-9_\-]+\.)+(mobi|[a-z]{2,3})");
    body = body.replaceAllMapped(regExpUrl, (match) => "");
    var length = (width - 300) / 2;
    if (body.length > length) {
      body = body.substring(0, length.toInt() - 1);
      body = "$body...";
    }
    return Text(body);
  }

  Widget _titleBody(double width) {
    return Expanded(
      child: SizedBox(
        width: width - 10 - 140,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              item.title,
              style: const TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 16,
              ),
            ),
            const SizedBox(height: 10),
            _body(width),
          ],
        ),
      ),
    );
  }

  Widget _extraRow(double width) {
    var timeAgo =
        timeago.format(DateTime.tryParse(item.created ?? '') ?? DateTime.now());
    var value =
        double.parse(item.pendingPayoutValue?.replaceAll(" HBD", "") ?? "0.0");
    return Row(
      children: [
        const Icon(Icons.arrow_circle_up_outlined, color: Colors.blueGrey),
        const SizedBox(width: 6),
        Text("\$ ${value.toStringAsFixed(2)}"),
        const SizedBox(width: 10),
        const Icon(Icons.favorite, color: Colors.blueGrey),
        const SizedBox(width: 6),
        Text("${item.activeVotes?.length ?? 0}"),
        const SizedBox(width: 10),
        const Icon(Icons.comment, color: Colors.blueGrey),
        const SizedBox(width: 6),
        Text("${item.children ?? 0}"),
        const SizedBox(width: 10),
        const Icon(Icons.lock_clock, color: Colors.blueGrey),
        const SizedBox(width: 6),
        Text(timeAgo),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width;
    String readTime = readingTime(item.body ?? '').msg;
    readTime = readTime == "less than a minute" ? "1 min read" : readTime;
    return Card(
      child: Container(
        margin: const EdgeInsets.all(5),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            !isUserChannel ? _headerRow() : Container(),
            const SizedBox(height: 10),
            InkWell(
              onTap: () {
                onPostTapped();
              },
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: shouldShowPostImage
                    ? [
                        _leftImage(),
                        const SizedBox(width: 10),
                        _titleBody(width),
                      ]
                    : [_titleBody(width)],
              ),
            ),
            const SizedBox(height: 10),
            _extraRow(width),
            // (<[^>]+>|\r|\n) - regex to remove html text
          ],
        ),
      ),
    );
  }
}
