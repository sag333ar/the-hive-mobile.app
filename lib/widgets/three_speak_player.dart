import 'package:flutter/material.dart';
import 'package:http/http.dart' show get;
import 'package:the_hive_app/utilities/models/video_details.dart';
import 'package:the_hive_app/widgets/video_player.dart';

class ThreeSpeakPlayer extends StatefulWidget {
  const ThreeSpeakPlayer({
    Key? key,
    required this.author,
    required this.permlink,
  }) : super(key: key);
  final String author;
  final String permlink;

  @override
  State<ThreeSpeakPlayer> createState() => _ThreeSpeakPlayerState();
}

class _ThreeSpeakPlayerState extends State<ThreeSpeakPlayer> {
  late String endPoint;
  late Future<VideoDetails> _loadDataFuture;

  @override
  void initState() {
    super.initState();
    endPoint = "https://3speak.tv/apiv2/@${widget.author}/${widget.permlink}";
    _loadDataFuture = _getVideoDetails();
  }

  Future<VideoDetails> _getVideoDetails() async {
    var response = await get(Uri.parse(endPoint));
    if (response.statusCode == 200) {
      VideoDetails data = VideoDetails.fromJsonString(response.body);
      return data;
    } else {
      throw "Status code = ${response.statusCode}";
    }
  }

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: 400,
      height: 225,
      child: FutureBuilder(
        future: _loadDataFuture,
        builder: (builder, snapshot) {
          if (snapshot.hasError) {
            return const Center(child: Text('Something went wrong'));
          } else if (snapshot.hasData &&
              snapshot.connectionState == ConnectionState.done) {
            var data = snapshot.data as VideoDetails;
            return HiveVideoPlayer(
              url: data.playUrl.replaceAll("default.m3u8", "480p.m3u8"),
            );
          } else {
            return const Center(child: CircularProgressIndicator());
          }
        },
      ),
    );
  }
}
