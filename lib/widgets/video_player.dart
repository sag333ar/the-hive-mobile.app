import 'dart:developer';

import 'package:chewie/chewie.dart';
import 'package:flutter/material.dart';
import 'package:video_player/video_player.dart';

class HiveVideoPlayer extends StatefulWidget {
  const HiveVideoPlayer({
    Key? key,
    required this.url,
  }) : super(key: key);
  final String url;

  @override
  State<HiveVideoPlayer> createState() => _HiveVideoPlayerState();
}

class _HiveVideoPlayerState extends State<HiveVideoPlayer> {
  late VideoPlayerController _controller;
  ChewieController? chewieController;

  @override
  void initState() {
    super.initState();
    log('Video player url - ${widget.url}');
    _controller = VideoPlayerController.network(
        //'https://ipfs-3speak.b-cdn.net/ipfs/QmbHNTrR4fPn3ayJ4dgAeLaZuDWndWT6HhuPJdRC3RmXvL/manifest.m3u8')
        widget.url)
      ..initialize().then((_) {
        _createChewieController();
        setState(() {});
      });
  }

  void _createChewieController() {
    chewieController = ChewieController(
      videoPlayerController: _controller,
      autoPlay: true,
      looping: true,
      hideControlsTimer: const Duration(seconds: 1),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: chewieController != null &&
              chewieController!.videoPlayerController.value.isInitialized
          ? Chewie(controller: chewieController!)
          : Container(),
    );
  }

  @override
  void dispose() {
    super.dispose();
    _controller.dispose();
  }
}
