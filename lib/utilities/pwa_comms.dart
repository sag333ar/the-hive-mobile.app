import 'package:js/js.dart';

@JS('getRedirectUriData')
external dynamic getRedirectUriData(
  String uname,
);

@JS('getDecryptedHASToken')
external dynamic getDecryptedHASToken(
  String username,
  String authKey,
  String data,
);

@JS('getEncryptedData')
external dynamic getEncryptedData(
  String authKey,
  String author,
  String token,
  String parentAuthor,
  String parentPermlink,
  String title,
  String body,
  String tags,
);

@JS('hiveKeychainSign')
external dynamic hiveKeychainSign(
  String username,
);

@JS('validateHiveKey')
external dynamic validateHiveKey(
  String username,
  String postingKey,
);

@JS('hasKeychainOnWindow')
external dynamic hasKeychainOnWindow();

@JS('sendMessage')
external dynamic sendMessage(
  String from,
  String to,
  String memo,
    bool isHive,
);

@JS('getDecodedMemo')
external dynamic getDecodedMemo(
  String from,
  String memo,
);
