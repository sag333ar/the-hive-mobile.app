import 'dart:convert';

import 'package:the_hive_app/utilities/models/safe_convert.dart';

class AccountDetailsResponse {
  final int id;
  final String jsonrpc;
  final List<AccountDetailsData> result;

  AccountDetailsResponse({
    this.id = 0,
    this.jsonrpc = "",
    required this.result,
  });

  factory AccountDetailsResponse.fromJson(Map<String, dynamic>? json) => AccountDetailsResponse(
    id: asInt(json, 'id'),
    jsonrpc: asString(json, 'jsonrpc'),
    result: asList(json, 'result').map((e) => AccountDetailsData.fromJson(e)).toList(),
  );

  factory AccountDetailsResponse.fromJsonString(String jsonString) =>
      AccountDetailsResponse.fromJson(json.decode(jsonString));
}

class AccountDetailsData {
  final int id;
  final String name;
  final AccountDetailsDataOwner owner;
  final AccountDetailsDataActive active;
  final AccountDetailsDataPosting posting;
  final String memoKey;
  final String jsonMetadata;
  final String postingJsonMetadata;
  final String proxy;
  final String previousOwnerUpdate;
  final String lastOwnerUpdate;
  final String lastAccountUpdate;
  final String created;
  final bool mined;
  final String recoveryAccount;
  final String lastAccountRecovery;
  final String resetAccount;
  final int commentCount;
  final int lifetimeVoteCount;
  final int postCount;
  final bool canVote;
  final VotingManabar votingManabar;
  final DownvoteManabar downvoteManabar;
  final int votingPower;
  final String balance;
  final String savingsBalance;
  final String hbdBalance;
  final String hbdSeconds;
  final String hbdSecondsLastUpdate;
  final String hbdLastInterestPayment;
  final String savingsHbdBalance;
  final String savingsHbdSeconds;
  final String savingsHbdSecondsLastUpdate;
  final String savingsHbdLastInterestPayment;
  final int savingsWithdrawRequests;
  final String rewardHbdBalance;
  final String rewardHiveBalance;
  final String rewardVestingBalance;
  final String rewardVestingHive;
  final String vestingShares;
  final String delegatedVestingShares;
  final String receivedVestingShares;
  final String vestingWithdrawRate;
  final String postVotingPower;
  final String nextVestingWithdrawal;
  final int withdrawn;
  final int toWithdraw;
  final int withdrawRoutes;
  final int pendingTransfers;
  final int curationRewards;
  final int postingRewards;
  // final List<Int> proxiedVsfVotes;
  final int witnessesVotedFor;
  final String lastPost;
  final String lastRootPost;
  final String lastVoteTime;
  final int postBandwidth;
  final int pendingClaimedAccounts;
  final String governanceVoteExpirationTs;
  final List<DelayedVotesItem> delayedVotes;
  final int openRecurrentTransfers;
  final String vestingBalance;
  final String reputation;
  // final List<Dynamic> transferHistory;
  // final List<Dynamic> marketHistory;
  // final List<Dynamic> postHistory;
  // final List<Dynamic> voteHistory;
  // final List<Dynamic> otherHistory;
  final List<String> witnessVotes;
  // final List<Dynamic> tagsUsage;
  // final List<Dynamic> guestBloggers;

  AccountDetailsData({
    this.id = 0,
    this.name = "",
    required this.owner,
    required this.active,
    required this.posting,
    this.memoKey = "",
    this.jsonMetadata = "",
    this.postingJsonMetadata = "",
    this.proxy = "",
    this.previousOwnerUpdate = "",
    this.lastOwnerUpdate = "",
    this.lastAccountUpdate = "",
    this.created = "",
    this.mined = false,
    this.recoveryAccount = "",
    this.lastAccountRecovery = "",
    this.resetAccount = "",
    this.commentCount = 0,
    this.lifetimeVoteCount = 0,
    this.postCount = 0,
    this.canVote = false,
    required this.votingManabar,
    required this.downvoteManabar,
    this.votingPower = 0,
    this.balance = "",
    this.savingsBalance = "",
    this.hbdBalance = "",
    this.hbdSeconds = "",
    this.hbdSecondsLastUpdate = "",
    this.hbdLastInterestPayment = "",
    this.savingsHbdBalance = "",
    this.savingsHbdSeconds = "",
    this.savingsHbdSecondsLastUpdate = "",
    this.savingsHbdLastInterestPayment = "",
    this.savingsWithdrawRequests = 0,
    this.rewardHbdBalance = "",
    this.rewardHiveBalance = "",
    this.rewardVestingBalance = "",
    this.rewardVestingHive = "",
    this.vestingShares = "",
    this.delegatedVestingShares = "",
    this.receivedVestingShares = "",
    this.vestingWithdrawRate = "",
    this.postVotingPower = "",
    this.nextVestingWithdrawal = "",
    this.withdrawn = 0,
    this.toWithdraw = 0,
    this.withdrawRoutes = 0,
    this.pendingTransfers = 0,
    this.curationRewards = 0,
    this.postingRewards = 0,
    // required this.proxiedVsfVotes,
    this.witnessesVotedFor = 0,
    this.lastPost = "",
    this.lastRootPost = "",
    this.lastVoteTime = "",
    this.postBandwidth = 0,
    this.pendingClaimedAccounts = 0,
    this.governanceVoteExpirationTs = "",
    required this.delayedVotes,
    this.openRecurrentTransfers = 0,
    this.vestingBalance = "",
    this.reputation = "",
    // required this.transferHistory,
    // required this.marketHistory,
    // required this.postHistory,
    // required this.voteHistory,
    // required this.otherHistory,
    required this.witnessVotes,
    // required this.tagsUsage,
    // required this.guestBloggers,
  });

  factory AccountDetailsData.fromJson(Map<String, dynamic>? json) => AccountDetailsData(
    id: asInt(json, 'id'),
    name: asString(json, 'name'),
    owner: AccountDetailsDataOwner.fromJson(asMap(json, 'owner')),
    active: AccountDetailsDataActive.fromJson(asMap(json, 'active')),
    posting: AccountDetailsDataPosting.fromJson(asMap(json, 'posting')),
    memoKey: asString(json, 'memo_key'),
    jsonMetadata: asString(json, 'json_metadata'),
    postingJsonMetadata: asString(json, 'posting_json_metadata'),
    proxy: asString(json, 'proxy'),
    previousOwnerUpdate: asString(json, 'previous_owner_update'),
    lastOwnerUpdate: asString(json, 'last_owner_update'),
    lastAccountUpdate: asString(json, 'last_account_update'),
    created: asString(json, 'created'),
    mined: asBool(json, 'mined'),
    recoveryAccount: asString(json, 'recovery_account'),
    lastAccountRecovery: asString(json, 'last_account_recovery'),
    resetAccount: asString(json, 'reset_account'),
    commentCount: asInt(json, 'comment_count'),
    lifetimeVoteCount: asInt(json, 'lifetime_vote_count'),
    postCount: asInt(json, 'post_count'),
    canVote: asBool(json, 'can_vote'),
    votingManabar: VotingManabar.fromJson(asMap(json, 'voting_manabar')),
    downvoteManabar: DownvoteManabar.fromJson(asMap(json, 'downvote_manabar')),
    votingPower: asInt(json, 'voting_power'),
    balance: asString(json, 'balance'),
    savingsBalance: asString(json, 'savings_balance'),
    hbdBalance: asString(json, 'hbd_balance'),
    hbdSeconds: asString(json, 'hbd_seconds'),
    hbdSecondsLastUpdate: asString(json, 'hbd_seconds_last_update'),
    hbdLastInterestPayment: asString(json, 'hbd_last_interest_payment'),
    savingsHbdBalance: asString(json, 'savings_hbd_balance'),
    savingsHbdSeconds: asString(json, 'savings_hbd_seconds'),
    savingsHbdSecondsLastUpdate: asString(json, 'savings_hbd_seconds_last_update'),
    savingsHbdLastInterestPayment: asString(json, 'savings_hbd_last_interest_payment'),
    savingsWithdrawRequests: asInt(json, 'savings_withdraw_requests'),
    rewardHbdBalance: asString(json, 'reward_hbd_balance'),
    rewardHiveBalance: asString(json, 'reward_hive_balance'),
    rewardVestingBalance: asString(json, 'reward_vesting_balance'),
    rewardVestingHive: asString(json, 'reward_vesting_hive'),
    vestingShares: asString(json, 'vesting_shares'),
    delegatedVestingShares: asString(json, 'delegated_vesting_shares'),
    receivedVestingShares: asString(json, 'received_vesting_shares'),
    vestingWithdrawRate: asString(json, 'vesting_withdraw_rate'),
    postVotingPower: asString(json, 'post_voting_power'),
    nextVestingWithdrawal: asString(json, 'next_vesting_withdrawal'),
    withdrawn: asInt(json, 'withdrawn'),
    toWithdraw: asInt(json, 'to_withdraw'),
    withdrawRoutes: asInt(json, 'withdraw_routes'),
    pendingTransfers: asInt(json, 'pending_transfers'),
    curationRewards: asInt(json, 'curation_rewards'),
    postingRewards: asInt(json, 'posting_rewards'),
    // proxiedVsfVotes: asList(json, 'proxied_vsf_votes').map((e) => int.tryParse(e.toString()) ?? 0).toList(),
    witnessesVotedFor: asInt(json, 'witnesses_voted_for'),
    lastPost: asString(json, 'last_post'),
    lastRootPost: asString(json, 'last_root_post'),
    lastVoteTime: asString(json, 'last_vote_time'),
    postBandwidth: asInt(json, 'post_bandwidth'),
    pendingClaimedAccounts: asInt(json, 'pending_claimed_accounts'),
    governanceVoteExpirationTs: asString(json, 'governance_vote_expiration_ts'),
    delayedVotes: asList(json, 'delayed_votes').map((e) => DelayedVotesItem.fromJson(e)).toList(),
    openRecurrentTransfers: asInt(json, 'open_recurrent_transfers'),
    vestingBalance: asString(json, 'vesting_balance'),
    reputation: asString(json, 'reputation'),
    // transferHistory: asList(json, 'transfer_history').map((e) => e.toString()).toList(),
    // marketHistory: asList(json, 'market_history').map((e) => e.toString()).toList(),
    // postHistory: asList(json, 'post_history').map((e) => e.toString()).toList(),
    // voteHistory: asList(json, 'vote_history').map((e) => e.toString()).toList(),
    // otherHistory: asList(json, 'other_history').map((e) => e.toString()).toList(),
    witnessVotes: asList(json, 'witness_votes').map((e) => e.toString()).toList(),
    // tagsUsage: asList(json, 'tags_usage').map((e) => e.toString()).toList(),
    // guestBloggers: asList(json, 'guest_bloggers').map((e) => e.toString()).toList(),
  );
}

class AccountDetailsDataOwner {
  final int weightThreshold;
  // final List<AccountAuthsSubList> accountAuths;
  // final List<KeyAuthsSubList> keyAuths;

  AccountDetailsDataOwner({
    this.weightThreshold = 0,
    // required this.accountAuths,
    // required this.keyAuths,
  });

  factory AccountDetailsDataOwner.fromJson(Map<String, dynamic>? json) => AccountDetailsDataOwner(
    weightThreshold: asInt(json, 'weight_threshold'),
    // accountAuths: asList(json, 'account_auths').map((e) => e.toString()).toList(),
    // keyAuths: asList(json, 'key_auths').map((e) => e.toString()).toList(),
  );
}


class AccountDetailsDataActive {
  final int weightThreshold;
  // final List<AccountAuthsSubList> accountAuths;
  // final List<KeyAuthsSubList> keyAuths;

  AccountDetailsDataActive({
    this.weightThreshold = 0,
    // required this.accountAuths,
    // required this.keyAuths,
  });

  factory AccountDetailsDataActive.fromJson(Map<String, dynamic>? json) => AccountDetailsDataActive(
    weightThreshold: asInt(json, 'weight_threshold'),
    // accountAuths: asList(json, 'account_auths').map((e) => e.toString()).toList(),
    // keyAuths: asList(json, 'key_auths').map((e) => e.toString()).toList(),
  );
}


class AccountDetailsDataPosting {
  final int weightThreshold;
  // final List<AccountAuthsSubList> accountAuths;
  // final List<KeyAuthsSubList> keyAuths;

  AccountDetailsDataPosting({
    this.weightThreshold = 0,
    // required this.accountAuths,
    // required this.keyAuths,
  });

  factory AccountDetailsDataPosting.fromJson(Map<String, dynamic>? json) => AccountDetailsDataPosting(
    weightThreshold: asInt(json, 'weight_threshold'),
    // accountAuths: asList(json, 'account_auths').map((e) => e.toString()).toList(),
    // keyAuths: asList(json, 'key_auths').map((e) => e.toString()).toList(),
  );
}

class VotingManabar {
  final String currentMana;
  final int lastUpdateTime;

  VotingManabar({
    this.currentMana = "",
    this.lastUpdateTime = 0,
  });

  factory VotingManabar.fromJson(Map<String, dynamic>? json) => VotingManabar(
    currentMana: asString(json, 'current_mana'),
    lastUpdateTime: asInt(json, 'last_update_time'),
  );
}

class DownvoteManabar {
  final String currentMana;
  final int lastUpdateTime;

  DownvoteManabar({
    this.currentMana = "",
    this.lastUpdateTime = 0,
  });

  factory DownvoteManabar.fromJson(Map<String, dynamic>? json) => DownvoteManabar(
    currentMana: asString(json, 'current_mana'),
    lastUpdateTime: asInt(json, 'last_update_time'),
  );
}

class DelayedVotesItem {
  final String time;
  final String val;

  DelayedVotesItem({
    this.time = "",
    this.val = "",
  });

  factory DelayedVotesItem.fromJson(Map<String, dynamic>? json) => DelayedVotesItem(
    time: asString(json, 'time'),
    val: asString(json, 'val'),
  );
}

