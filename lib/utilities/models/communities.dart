import 'dart:convert';

import 'package:the_hive_app/utilities/models/safe_convert.dart';

class CommunitiesResponseModel {
  final String jsonrpc;
  final List<CommunityItem> result;
  final int id;

  CommunitiesResponseModel({
    this.jsonrpc = "",
    required this.result,
    this.id = 0,
  });

  factory CommunitiesResponseModel.fromJson(Map<String, dynamic>? json) =>
      CommunitiesResponseModel(
        jsonrpc: asString(json, 'jsonrpc'),
        result: asList(json, 'result')
            .map((e) => CommunityItem.fromJson(e))
            .toList(),
        id: asInt(json, 'id'),
      );

  factory CommunitiesResponseModel.fromJsonString(String jsonString) =>
      CommunitiesResponseModel.fromJson(json.decode(jsonString));
}

class CommunityItem {
  final int id;
  final String name;
  final String title;
  final String about;
  final String lang;
  final int typeId;
  final bool isNsfw;
  final int subscribers;
  final int sumPending;
  final int numPending;
  final int numAuthors;
  final String createdAt;
  final String avatarUrl;
  final CommunityItemContext context;
  final List<String> admins;

  CommunityItem({
    this.id = 0,
    this.name = "",
    this.title = "",
    this.about = "",
    this.lang = "",
    this.typeId = 0,
    this.isNsfw = false,
    this.subscribers = 0,
    this.sumPending = 0,
    this.numPending = 0,
    this.numAuthors = 0,
    this.createdAt = "",
    this.avatarUrl = "",
    required this.context,
    required this.admins,
  });

  factory CommunityItem.fromJson(Map<String, dynamic>? json) => CommunityItem(
        id: asInt(json, 'id'),
        name: asString(json, 'name'),
        title: asString(json, 'title'),
        about: asString(json, 'about'),
        lang: asString(json, 'lang'),
        typeId: asInt(json, 'type_id'),
        isNsfw: asBool(json, 'is_nsfw'),
        subscribers: asInt(json, 'subscribers'),
        sumPending: asInt(json, 'sum_pending'),
        numPending: asInt(json, 'num_pending'),
        numAuthors: asInt(json, 'num_authors'),
        createdAt: asString(json, 'created_at'),
        avatarUrl: asString(json, 'avatar_url'),
        context: CommunityItemContext.fromJson(asMap(json, 'context')),
        admins: asList(json, 'admins').map((e) => e.toString()).toList(),
      );
}

class CommunityItemContext {
  final String role;
  final bool subscribed;
  final String title;

  CommunityItemContext({
    this.role = "",
    this.subscribed = false,
    this.title = "",
  });

  factory CommunityItemContext.fromJson(Map<String, dynamic>? json) =>
      CommunityItemContext(
        role: asString(json, 'role'),
        subscribed: asBool(json, 'subscribed'),
        title: asString(json, 'title'),
      );
}
