import 'dart:convert';

import 'package:the_hive_app/utilities/models/safe_convert.dart';

class UserFollowersResponse {
  final String jsonrpc;
  final List<UserFollowersResponseItem> result;
  final int id;

  UserFollowersResponse({
    this.jsonrpc = "",
    required this.result,
    this.id = 0,
  });

  factory UserFollowersResponse.fromJson(Map<String, dynamic>? json) =>
      UserFollowersResponse(
        jsonrpc: asString(json, 'jsonrpc'),
        result:
            asList(json, 'result').map((e) => UserFollowersResponseItem.fromJson(e)).toList(),
        id: asInt(json, 'id'),
      );

  factory UserFollowersResponse.fromJsonString(String jsonString) =>
      UserFollowersResponse.fromJson(json.decode(jsonString));
}

class UserFollowersResponseItem {
  final String? follower;
  final String? following;
  final String? name;
  // final List<String> what;

  UserFollowersResponseItem({
    this.follower = "",
    this.following = "",
    this.name = ""
    // required this.what,
  });

  factory UserFollowersResponseItem.fromJson(Map<String, dynamic>? json) => UserFollowersResponseItem(
        follower: asString(json, 'follower'),
        following: asString(json, 'following'),
        name: asString(json, 'name'),
        // what: asList(json, 'what').map((e) => e.toString()).toList(),
      );
}
