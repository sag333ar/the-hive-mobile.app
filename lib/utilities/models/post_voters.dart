import 'dart:convert';

import 'package:the_hive_app/utilities/models/safe_convert.dart';

class PostVoters {
  final List<PostVoterItem> result;

  PostVoters({
    required this.result,
  });

  factory PostVoters.fromJson(Map<String, dynamic>? json) => PostVoters(
        result: asList(json, 'result')
            .map((e) => PostVoterItem.fromJson(e))
            .toList(),
      );

  factory PostVoters.fromJsonString(String jsonString) =>
      PostVoters.fromJson(json.decode(jsonString));
}

class PostVoterItem {
  final int percent;
  final int reputation;
  final int rshares;
  final String time;
  final String voter;
  final int weight;

  PostVoterItem({
    this.percent = 0,
    this.reputation = 0,
    this.rshares = 0,
    this.time = "",
    this.voter = "",
    this.weight = 0,
  });

  factory PostVoterItem.fromJson(Map<String, dynamic>? json) => PostVoterItem(
        percent: asInt(json, 'percent'),
        reputation: asInt(json, 'reputation'),
        rshares: asInt(json, 'rshares'),
        time: asString(json, 'time'),
        voter: asString(json, 'voter'),
        weight: asInt(json, 'weight'),
      );
}
