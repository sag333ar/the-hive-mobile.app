import 'dart:convert';

import 'package:the_hive_app/utilities/models/safe_convert.dart';
import 'package:localstore/localstore.dart';

class AccountHistoryForTransfers {
  final List<TransferEntry> items;
  final int firstBlock;

  AccountHistoryForTransfers({
    required this.items,
    required this.firstBlock,
  });

  factory AccountHistoryForTransfers.fromJsonString(String jsonString) =>
      AccountHistoryForTransfers.fromJson(json.decode(jsonString));

  factory AccountHistoryForTransfers.fromJson(Map<String, dynamic>? json) {
    var resultList = asList(json, 'result');
    if (resultList.isEmpty) {
      return AccountHistoryForTransfers(firstBlock: 0, items: []);
    }
    var operationsList = resultList.map((e) {
      var elementList = e as List<dynamic>;
      var map = elementList[1] as Map<String, dynamic>;
      return map;
    });
    var min = (resultList.first as List<dynamic>).first as int;
    var operations = operationsList.map((e) {
      var op = asList(e, 'op');
      var opData = op[1] as Map<String, dynamic>;
      var to = asString(opData, 'to');
      var from = asString(opData, 'from');
      var memo = asString(opData, 'memo');
      var amount = asString(opData, 'amount');
      var timestamp = asString(e, 'timestamp');
      var trxId = asString(e, 'trx_id');
      return TransferEntry(
        to: to,
        from: from,
        memo: memo,
        amount: amount,
        timestamp: timestamp,
        trxId: trxId,
      );
    }).toList();
    operations.sort((a, b) => a.timestamp.compareTo(b.timestamp));
    return AccountHistoryForTransfers(
      items: operations,
      firstBlock: min,
    );
  }
}

class TransferEntry {
  final String to;
  final String from;
  String memo;
  final String amount;
  final String timestamp;
  final String trxId;

  TransferEntry({
    required this.to,
    required this.from,
    required this.memo,
    required this.amount,
    required this.timestamp,
    required this.trxId,
  });

  Map<String, dynamic> toJson() => {
    'to': to,
    'from': from,
    'memo': memo,
    'amount': amount,
    'timestamp': timestamp,
    'trxId': trxId,
  };

  factory TransferEntry.fromJson(Map<String, dynamic>? json) => TransferEntry(
    to: asString(json, 'to'),
    from: asString(json, 'from'),
    memo: asString(json, 'memo'),
    amount: asString(json, 'amount'),
    timestamp: asString(json, 'timestamp'),
    trxId: asString(json, 'trxId'),
  );

  static Future<List<TransferEntry>> fromDB(String username) async {
    final db = Localstore.instance;
    final items = await db.collection('messages_$username').get();
    List<TransferEntry> messages = [];
    items?.forEach((key, value) {
      var post = value as Map<String, dynamic>;
      messages.add(TransferEntry.fromJson(post));
    });
    return messages;
  }

  static Future<void> toDB(TransferEntry entry, String username) async {
    final db = Localstore.instance;
    await db.collection('messages_$username').doc(entry.trxId).set(entry.toJson());
  }
}
