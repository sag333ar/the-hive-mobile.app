import 'dart:convert';

import 'package:the_hive_app/utilities/models/safe_convert.dart';

class UserCommunitySubscriptions {
  final List<List<String>> result;

  UserCommunitySubscriptions({
    required this.result,
  });

  factory UserCommunitySubscriptions.fromJson(Map<String, dynamic>? json) {
    var list = asList(json, 'result');
    List<List<String>> stringList = [];
    for (var element in list) {
      var innerList = element as List<dynamic>;
      stringList.add(innerList.map((e) => e.toString()).toList());
    }
    return UserCommunitySubscriptions(
      result: stringList,
    );
  }

  factory UserCommunitySubscriptions.fromJsonString(String jsonString) =>
      UserCommunitySubscriptions.fromJson(json.decode(jsonString));
}
