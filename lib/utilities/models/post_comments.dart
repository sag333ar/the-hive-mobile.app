import 'dart:convert';
import 'package:the_hive_app/utilities/models/safe_convert.dart';
import 'package:the_hive_app/utilities/models/post_voters.dart';

HiveComments hiveCommentsFromString(
  String string,
  String author,
  String permlink,
) {
  return HiveComments.fromJson(json.decode(string), author, permlink);
}

class HiveComments {
  final String jsonrpc;
  final List<HiveComment> result;
  final int id;

  HiveComments({
    this.jsonrpc = "",
    required this.result,
    this.id = 0,
  });

  factory HiveComments.fromJson(Map<String, dynamic>? json, String author, String permlink) {
    var jsonMap = asMap(json, 'result');
    var keys = jsonMap.keys.toList();
    List<HiveComment> comments = [];
    for (var element in keys) {
      if (element != "$author/$permlink") {
        var map = asMap(jsonMap, element);
        comments.add(HiveComment.fromJson(map));
      }
    }
    return HiveComments(
      jsonrpc: "2.0",
      result: comments,
      id: 50,
    );
  }
}

class HiveComment {
  final String author;
  final String permlink;
  final String category;
  final String body;
  final String created;
  final int depth;
  final int children;
  final String lastPayout;
  final String cashoutTime;
  final String totalPayoutValue;
  final String curatorPayoutValue;
  final String pendingPayoutValue;
  final String parentAuthor;
  final String parentPermlink;
  final String url;
  final List<PostVoterItem> activeVotes;
  final int? authorReputation;
  final int? netRshares;

  HiveComment({
    this.author = "",
    this.permlink = "",
    this.category = "",
    this.body = "",
    this.created = "",
    this.depth = 0,
    this.children = 0,
    this.lastPayout = "",
    this.cashoutTime = "",
    this.totalPayoutValue = "",
    this.curatorPayoutValue = "",
    this.pendingPayoutValue = "",
    this.parentAuthor = "",
    this.parentPermlink = "",
    this.url = "",
    required this.activeVotes,
    required this.authorReputation,
    required this.netRshares,
  });

  DateTime? get createdAt {
    return DateTime.tryParse(created);
  }

  factory HiveComment.fromJson(Map<String, dynamic>? json) => HiveComment(
        author: asString(json, 'author'),
        permlink: asString(json, 'permlink'),
        category: asString(json, 'category'),
        body: asString(json, 'body'),
        created: asString(json, 'created'),
        depth: asInt(json, 'depth'),
        children: asInt(json, 'children'),
        lastPayout: asString(json, 'last_payout'),
        totalPayoutValue: asString(json, 'total_payout_value'),
        pendingPayoutValue: asString(json, 'pending_payout_value'),
        parentAuthor: asString(json, 'parent_author'),
        parentPermlink: asString(json, 'parent_permlink'),
        url: asString(json, 'url'),
        authorReputation: asInt(json, 'author_reputation'),
        netRshares: asInt(json, 'net_rshares'),
        activeVotes: asList(json, 'active_votes')
            .map((e) => PostVoterItem.fromJson(json))
            .toList(),
      );
}
