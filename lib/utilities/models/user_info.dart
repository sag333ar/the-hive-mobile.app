import 'dart:convert';

import 'package:the_hive_app/utilities/models/safe_convert.dart';
import 'package:timeago/timeago.dart' as timeago;

class UserInfoResponseModel {
  final String jsonrpc;
  final UserInfoResponseModelResult result;
  final int id;

  UserInfoResponseModel({
    this.jsonrpc = "",
    required this.result,
    this.id = 0,
  });

  factory UserInfoResponseModel.fromJson(Map<String, dynamic>? json) =>
      UserInfoResponseModel(
        jsonrpc: asString(json, 'jsonrpc'),
        result: UserInfoResponseModelResult.fromJson(asMap(json, 'result')),
        id: asInt(json, 'id'),
      );

  factory UserInfoResponseModel.fromJsonString(String jsonString) =>
      UserInfoResponseModel.fromJson(json.decode(jsonString));

  Map<String, dynamic> toJson() => {
        'jsonrpc': jsonrpc,
        'result': result.toJson(),
        'id': id,
      };
}

class UserInfoResponseModelResult {
  final int id;
  final String name;
  final String created;
  final String active;
  final int postCount;
  final double reputation;

  // final List<Dynamic> blacklists;
  final UserInfoResponseModelResultStats stats;
  final UserInfoResponseModelResultMetadata metadata;

  // final Context context;

  UserInfoResponseModelResult({
    this.id = 0,
    this.name = "",
    this.created = "",
    this.active = "",
    this.postCount = 0,
    this.reputation = 0.0,
    // required this.blacklists,
    required this.stats,
    required this.metadata,
    // required this.context,
  });

  String activeAgo() {
    var activeDateTime = DateTime.tryParse(active);
    if (activeDateTime == null) {
      return "Unknown";
    }
    return timeago.format(activeDateTime, locale: DateTime.now().timeZoneName);
  }

  String joinedAgo() {
    var joinedDateTime = DateTime.tryParse(created);
    if (joinedDateTime == null) {
      return "Unknown";
    }
    return timeago.format(joinedDateTime, locale: DateTime.now().timeZoneName);
  }

  String getCoverImage() {
    return metadata.profile.coverImage.isEmpty ? "https://images.hive.blog/0x0/https://files.peakd.com/file/peakd-hive/sagarkothari88/sagar.kothari.png" : metadata.profile.coverImage;
  }

  String getProfileImage() {
    return metadata.profile.profileImage.isEmpty ? "https://assets.dapp.review/dapp-logo/2020.06.16/jbrHiwhEjti86bRbjMdr4zdb3sTJn2Hd.png" : metadata.profile.profileImage;
  }

  factory UserInfoResponseModelResult.fromJson(Map<String, dynamic>? json) =>
      UserInfoResponseModelResult(
        id: asInt(json, 'id'),
        name: asString(json, 'name'),
        created: asString(json, 'created'),
        active: asString(json, 'active'),
        postCount: asInt(json, 'post_count'),
        reputation: asDouble(json, 'reputation'),
        // blacklists: asList(json, 'blacklists').map((e) => e.toString()).toList(),
        stats: UserInfoResponseModelResultStats.fromJson(asMap(json, 'stats')),
        metadata: UserInfoResponseModelResultMetadata.fromJson(
            asMap(json, 'metadata')),
        // context: Context.fromJson(asMap(json, 'context')),
      );

  Map<String, dynamic> toJson() => {
        'id': id,
        'name': name,
        'created': created,
        'active': active,
        'post_count': postCount,
        'reputation': reputation,
        // 'blacklists': blacklists.map((e) => e).toList(),
        'stats': stats.toJson(),
        'metadata': metadata.toJson(),
        // 'context': context.toJson(),
      };
}

class UserInfoResponseModelResultStats {
  final int rank;
  final int following;
  final int followers;

  UserInfoResponseModelResultStats({
    this.rank = 0,
    this.following = 0,
    this.followers = 0,
  });

  factory UserInfoResponseModelResultStats.fromJson(
          Map<String, dynamic>? json) =>
      UserInfoResponseModelResultStats(
        rank: asInt(json, 'rank'),
        following: asInt(json, 'following'),
        followers: asInt(json, 'followers'),
      );

  Map<String, dynamic> toJson() => {
        'rank': rank,
        'following': following,
        'followers': followers,
      };
}

class UserInfoResponseModelResultMetadata {
  final UserProfile profile;

  UserInfoResponseModelResultMetadata({
    required this.profile,
  });

  factory UserInfoResponseModelResultMetadata.fromJson(
          Map<String, dynamic>? json) =>
      UserInfoResponseModelResultMetadata(
        profile: UserProfile.fromJson(asMap(json, 'profile')),
      );

  Map<String, dynamic> toJson() => {
        'profile': profile.toJson(),
      };
}

class UserProfile {
  final String name;
  final String about;
  final String website;
  final String location;
  final String coverImage;
  final String profileImage;
  final String blacklistDescription;
  final String mutedListDescription;

  UserProfile({
    this.name = "",
    this.about = "",
    this.website = "",
    this.location = "",
    this.coverImage = "",
    this.profileImage = "",
    this.blacklistDescription = "",
    this.mutedListDescription = "",
  });

  factory UserProfile.fromJson(Map<String, dynamic>? json) => UserProfile(
        name: asString(json, 'name'),
        about: asString(json, 'about'),
        website: asString(json, 'website'),
        location: asString(json, 'location'),
        coverImage: asString(json, 'cover_image'),
        profileImage: asString(json, 'profile_image'),
        blacklistDescription: asString(json, 'blacklist_description'),
        mutedListDescription: asString(json, 'muted_list_description'),
      );

  Map<String, dynamic> toJson() => {
        'name': name,
        'about': about,
        'website': website,
        'location': location,
        'cover_image': coverImage,
        'profile_image': profileImage,
        'blacklist_description': blacklistDescription,
        'muted_list_description': mutedListDescription,
      };
}

// class Context {
//   final bool followed;
//
//   Context({
//     this.followed = false,
//   });
//
//   factory Context.fromJson(Map<String, dynamic>? json) => Context(
//     followed: asBool(json, 'followed'),
//   );
//
//   Map<String, dynamic> toJson() => {
//     'followed': followed,
//   };
// }
