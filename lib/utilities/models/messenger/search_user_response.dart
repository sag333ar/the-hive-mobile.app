import 'dart:convert';

import 'package:the_hive_app/utilities/models/safe_convert.dart';

class SearchUserResponse {
  final List<String> result;

  SearchUserResponse({
    required this.result,
  });

  factory SearchUserResponse.fromJson(Map<String, dynamic>? json) =>
      SearchUserResponse(
        result: asList(json, 'result').map((e) => e.toString()).toList(),
      );

  factory SearchUserResponse.fromString(String jsonString) =>
      SearchUserResponse.fromJson(json.decode(jsonString));
}
