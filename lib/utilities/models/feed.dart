import 'dart:convert';

import 'package:the_hive_app/utilities/models/safe_convert.dart';

class FeedResponse {
  final List<HivePostJsonItem> result;

  FeedResponse({
    required this.result,
  });

  factory FeedResponse.fromJson(Map<String, dynamic>? json) => FeedResponse(
        result: asList(json, 'result')
            .map((e) => HivePostJsonItem.fromJson(e))
            .toList(),
      );

  factory FeedResponse.fromJsonString(String jsonString) =>
      FeedResponse.fromJson(json.decode(jsonString));
}

class SingleItemResponse {
  final HivePostJsonItem content;

  SingleItemResponse({
    required this.content,
  });

  factory SingleItemResponse.fromJsonForKey(
      {required Map<String, dynamic>? json, required String forKey}) {
    var result = asMap(json, 'result');
    var content = asMap(
      result,
      result.keys.firstWhere((element) => element == forKey),
    );
    return SingleItemResponse(content: HivePostJsonItem.fromJson(content));
  }

  factory SingleItemResponse.fromJsonStringForKey({
    required String string,
    required String forKey,
  }) =>
      SingleItemResponse.fromJsonForKey(
        json: json.decode(string),
        forKey: forKey,
      );
}

class HivePostJsonItem {
  // final int postId;
  final String? author;
  final String permlink;
  final String? category;
  final String title;
  final String? body;
  final PostJsonMetadata? jsonMetadata;
  final String? created;
  // final String updated;
  final int? depth;
  final int? children;
  final int netRshares;
  // final bool isPaidout;
  // final String payoutAt;
  final double? payout;
  final String? pendingPayoutValue;
  final String? authorPayoutValue;
  final String? curatorPayoutValue;
  final String? promoted;
  // final List<Dynamic> replies;
  final double? authorReputation;
  // final PostStats stats;
  final String url;
  // final List<Dynamic> beneficiaries;
  final String? maxAcceptedPayout;
  final int? percentHbd;
  final String? parentAuthor;
  final String? parentPermlink;
  final List<PostActiveVotesItem>? activeVotes;
  // final List<Dynamic> blacklists;
  final String? community;
  final String? communityTitle;
  final String? authorRole;
  final String? authorTitle;

  HivePostJsonItem({
    // this.postId = 0,
    this.author = "",
    this.permlink = "",
    this.category = "",
    this.title = "",
    this.body = "",
    required this.jsonMetadata,
    this.created = "",
    // this.updated = "",
    this.depth = 0,
    this.children = 0,
    this.netRshares = 0,
    // this.isPaidout = false,
    // this.payoutAt = "",
    this.payout = 0.0,
    this.pendingPayoutValue = "",
    this.authorPayoutValue = "",
    this.curatorPayoutValue = "",
    this.promoted = "",
    // required this.replies,
    this.authorReputation = 0.0,
    // required this.stats,
    this.url = "",
    // required this.beneficiaries,
    this.maxAcceptedPayout = "",
    this.percentHbd = 0,
    this.parentAuthor = "",
    this.parentPermlink = "",
    required this.activeVotes,
    // required this.blacklists,
    this.community = "",
    this.communityTitle = "",
    this.authorRole = "",
    this.authorTitle = "",
  });

  factory HivePostJsonItem.fromJson(Map<String, dynamic>? json) =>
      HivePostJsonItem(
        // postId: asInt(json, 'post_id'),
        author: asString(json, 'author'),
        permlink: asString(json, 'permlink'),
        category: asString(json, 'category'),
        title: asString(json, 'title'),
        body: asString(json, 'body'),
        jsonMetadata: PostJsonMetadata.fromJson(asMap(json, 'json_metadata')),
        created: asString(json, 'created'),
        // updated: asString(json, 'updated'),
        depth: asInt(json, 'depth'),
        children: asInt(json, 'children'),
        netRshares: asInt(json, 'net_rshares'),
        // isPaidout: asBool(json, 'is_paidout'),
        // payoutAt: asString(json, 'payout_at'),
        payout: asDouble(json, 'payout'),
        pendingPayoutValue: asString(json, 'pending_payout_value'),
        authorPayoutValue: asString(json, 'author_payout_value'),
        curatorPayoutValue: asString(json, 'curator_payout_value'),
        promoted: asString(json, 'promoted'),
        // replies: asList(json, 'replies').map((e) => e.toString()).toList(),
        authorReputation: asDouble(json, 'author_reputation'),
        // stats: PostStats.fromJson(asMap(json, 'stats')),
        url: asString(json, 'url'),
        // beneficiaries:
        //     asList(json, 'beneficiaries').map((e) => e.toString()).toList(),
        maxAcceptedPayout: asString(json, 'max_accepted_payout'),
        percentHbd: asInt(json, 'percent_hbd'),
        parentAuthor: asString(json, 'parent_author'),
        parentPermlink: asString(json, 'parent_permlink'),
        activeVotes: asList(json, 'active_votes')
            .map((e) => PostActiveVotesItem.fromJson(e))
            .toList(),
        // blacklists:
        //     asList(json, 'blacklists').map((e) => e.toString()).toList(),
        community: asString(json, 'community'),
        communityTitle: asString(json, 'community_title'),
        authorRole: asString(json, 'author_role'),
        authorTitle: asString(json, 'author_title'),
      );
}

class PostJsonMetadata {
  final List<String>? tags;
  final List<String>? image;
  final String? app;
  final PostJsonVideo? video;

  PostJsonMetadata({
    required this.tags,
    required this.image,
    this.app = "",
    required this.video,
  });

  factory PostJsonMetadata.fromJson(Map<String, dynamic>? json) =>
      PostJsonMetadata(
        tags: asList(json, 'tags').map((e) => e.toString()).toList(),
        image: asList(json, 'image').map((e) => e.toString()).toList(),
        app: asString(json, 'app'),
        video: PostJsonVideo.fromJson(
          asMap(json, 'video'),
        ),
      );
}

class PostJsonVideo {
  final PostJsonVideoInfo info;

  PostJsonVideo({
    required this.info,
  });

  factory PostJsonVideo.fromJson(Map<String, dynamic>? json) => PostJsonVideo(
        info: PostJsonVideoInfo.fromJson(
          asMap(json, 'info'),
        ),
      );
}

class PostJsonVideoInfo {
  final String? videoV2;
  PostJsonVideoInfo({
    required this.videoV2,
  });

  factory PostJsonVideoInfo.fromJson(Map<String, dynamic>? json) =>
      PostJsonVideoInfo(
        videoV2: asString(json, 'videoV2'),
      );
}

class PostStats {
  final bool? hide;
  final bool? gray;
  final int? totalVotes;
  final double? flagWeight;

  PostStats({
    this.hide = false,
    this.gray = false,
    this.totalVotes = 0,
    this.flagWeight = 0.0,
  });

  factory PostStats.fromJson(Map<String, dynamic>? json) => PostStats(
        hide: asBool(json, 'hide'),
        gray: asBool(json, 'gray'),
        totalVotes: asInt(json, 'total_votes'),
        flagWeight: asDouble(json, 'flag_weight'),
      );
}

class PostActiveVotesItem {
  final int rshares;
  final String voter;

  PostActiveVotesItem({
    this.rshares = 0,
    this.voter = "",
  });

  factory PostActiveVotesItem.fromJson(Map<String, dynamic>? json) =>
      PostActiveVotesItem(
        rshares: asInt(json, 'rshares'),
        voter: asString(json, 'voter'),
      );
}
