import 'dart:convert';

import 'package:the_hive_app/utilities/models/safe_convert.dart';

class PeakdBadgesItem {
  final String name;
  final String createdAt;
  final String reputation;
  final int followers;
  final int following;

  static List<PeakdBadgesItem> fromJsonString(String jsonString) {
    final jsonList = json.decode(jsonString) as List;
    return jsonList.map((e) => PeakdBadgesItem.fromJson(e)).toList();
  }
  final String title;

  PeakdBadgesItem({
    this.name = "",
    this.createdAt = "",
    this.reputation = "",
    this.followers = 0,
    this.following = 0,
    this.title = "",
  });

  factory PeakdBadgesItem.fromJson(Map<String, dynamic>? json) => PeakdBadgesItem(
    name: asString(json, 'name'),
    createdAt: asString(json, 'created_at'),
    reputation: asString(json, 'reputation'),
    followers: asInt(json, 'followers'),
    following: asInt(json, 'following'),
    title: asString(json, 'title'),
  );

  Map<String, dynamic> toJson() => {
    'name': name,
    'created_at': createdAt,
    'reputation': reputation,
    'followers': followers,
    'following': following,
    'title': title,
  };
}

