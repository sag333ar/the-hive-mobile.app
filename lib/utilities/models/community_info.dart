import 'dart:convert';

import 'package:the_hive_app/utilities/models/safe_convert.dart';

class CommunityInfoResponse {
  final CommunityInfoItem result;

  CommunityInfoResponse({
    required this.result,
  });

  factory CommunityInfoResponse.fromJson(Map<String, dynamic>? json) =>
      CommunityInfoResponse(
        result: CommunityInfoItem.fromJson(asMap(json, 'result')),
      );

  factory CommunityInfoResponse.fromJsonString(String jsonString) {
    var jsonData = json.decode(jsonString);
    return CommunityInfoResponse.fromJson(jsonData);
  }
}

class CommunityInfoItem {
  final int id;
  final String name;
  final String title;
  final String about;
  final String lang;
  final int typeId;
  final bool isNsfw;
  final int subscribers;
  final String createdAt;
  final int sumPending;
  final int numPending;
  final int numAuthors;
  final String avatarUrl;
  final String description;
  final String flagText;
  final List<List<String>> team;

  CommunityInfoItem({
    this.id = 0,
    this.name = "",
    this.title = "",
    this.about = "",
    this.lang = "",
    this.typeId = 0,
    this.isNsfw = false,
    this.subscribers = 0,
    this.createdAt = "",
    this.sumPending = 0,
    this.numPending = 0,
    this.numAuthors = 0,
    this.avatarUrl = "",
    this.description = "",
    this.flagText = "",
    required this.team,
  });

  factory CommunityInfoItem.fromJson(Map<String, dynamic>? json) {
    var values = asList(json, 'team');
    var team = values.map((e) {
      var list = e as List;
      return list.map((e) => e.toString()).toList();
    }).toList();
    return CommunityInfoItem(
      id: asInt(json, 'id'),
      name: asString(json, 'name'),
      title: asString(json, 'title'),
      about: asString(json, 'about'),
      lang: asString(json, 'lang'),
      typeId: asInt(json, 'type_id'),
      isNsfw: asBool(json, 'is_nsfw'),
      subscribers: asInt(json, 'subscribers'),
      createdAt: asString(json, 'created_at'),
      sumPending: asInt(json, 'sum_pending'),
      numPending: asInt(json, 'num_pending'),
      numAuthors: asInt(json, 'num_authors'),
      avatarUrl: asString(json, 'avatar_url'),
      description: asString(json, 'description'),
      flagText: asString(json, 'flag_text'),
      team: team,
    );
  }
}
