import 'dart:convert';

import 'package:the_hive_app/utilities/models/safe_convert.dart';

class HiveMessageResponse {
  final String message;
  final bool success;
  final HiveMessageResponseData? data;
  final HiveMessageResponseResult? result;

  HiveMessageResponse({
    required this.message,
    required this.success,
    required this.data,
    required this.result,
  });

  factory HiveMessageResponse.fromJson(Map<String, dynamic>? json) =>
      HiveMessageResponse(
        message: asString(json, 'message'),
        success: asBool(json, 'success'),
        data: HiveMessageResponseData.fromJson(json?['data']),
        result: HiveMessageResponseResult.fromJson(json?['result']),
      );

  factory HiveMessageResponse.fromString(String string) =>
      HiveMessageResponse.fromJson(json.decode(string));
}

class HiveMessageResponseResult {
  String id;
  String txId;

  HiveMessageResponseResult({
    required this.id,
    required this.txId,
  });

  factory HiveMessageResponseResult.fromJson(Map<String, dynamic>? json) =>
      HiveMessageResponseResult(
        id: asString(json, 'id'),
        txId: asString(json, 'tx_id'),
      );
}

class HiveMessageResponseData {
  final String amount;
  final String currency;
  final String memo;
  final String to;
  final String username;

  HiveMessageResponseData({
    required this.amount,
    required this.currency,
    required this.memo,
    required this.to,
    required this.username,
  });

  factory HiveMessageResponseData.fromJson(Map<String, dynamic>? json) =>
      HiveMessageResponseData(
        amount: asString(json, 'amount'),
        currency: asString(json, 'currency'),
        memo: asString(json, 'memo'),
        to: asString(json, 'to'),
        username: asString(json, 'username'),
      );
}