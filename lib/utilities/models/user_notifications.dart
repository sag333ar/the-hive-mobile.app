import 'dart:convert';

import 'package:the_hive_app/utilities/models/safe_convert.dart';

class UserNotifications {
  final List<UserNotificationItem> result;

  UserNotifications({
    required this.result,
  });

  factory UserNotifications.fromJson(Map<String, dynamic>? json) =>
      UserNotifications(
        result: asList(json, 'result')
            .map((e) => UserNotificationItem.fromJson(e))
            .toList(),
      );

  factory UserNotifications.fromJsonString(String jsonString) =>
      UserNotifications.fromJson(
        json.decode(jsonString),
      );
}

class UserNotificationItem {
  final int id;
  final String type;
  final int score;
  final String date;
  final String msg;
  final String url;

  UserNotificationItem({
    this.id = 0,
    this.type = "",
    this.score = 0,
    this.date = "",
    this.msg = "",
    this.url = "",
  });

  factory UserNotificationItem.fromJson(Map<String, dynamic>? json) =>
      UserNotificationItem(
        id: asInt(json, 'id'),
        type: asString(json, 'type'),
        score: asInt(json, 'score'),
        date: asString(json, 'date'),
        msg: asString(json, 'msg'),
        url: asString(json, 'url'),
      );
}
