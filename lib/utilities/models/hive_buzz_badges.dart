import 'dart:convert';

import 'package:the_hive_app/utilities/models/safe_convert.dart';

class HiveBuzzBadgesItem {
  final String type;
  final int id;
  final String name;
  final String title;
  final String description;
  final String descriptionTitle;
  final String group;
  final String groupname;
  final bool groupExpandable;
  final bool groupExpanded;
  final String state;
  final int? level;
  final String url;
  final int displayOrder;

  static List<HiveBuzzBadgesItem> fromJsonString(String jsonString) {
    final jsonList = json.decode(jsonString) as List;
    return jsonList.map((e) => HiveBuzzBadgesItem.fromJson(e)).toList();
  }

  HiveBuzzBadgesItem({
    this.type = "",
    this.id = 0,
    this.name = "",
    this.title = "",
    this.description = "",
    this.descriptionTitle = "",
    this.group = "",
    this.groupname = "",
    this.groupExpandable = false,
    this.groupExpanded = false,
    this.state = "",
    this.url = "",
    this.displayOrder = 0,
    this.level = 5000,
  });

  factory HiveBuzzBadgesItem.fromJson(Map<String, dynamic>? json) => HiveBuzzBadgesItem(
    type: asString(json, 'type'),
    id: asInt(json, 'id'),
    name: asString(json, 'name'),
    title: asString(json, 'title'),
    description: asString(json, 'description'),
    descriptionTitle: asString(json, 'description_title'),
    group: asString(json, 'group'),
    groupname: asString(json, 'groupname'),
    groupExpandable: asBool(json, 'group_expandable'),
    groupExpanded: asBool(json, 'group_expanded'),
    state: asString(json, 'state'),
    level: asInt(json, 'level', defaultValue: 5000),
    url: asString(json, 'url'),
    displayOrder: asInt(json, 'display_order'),
  );

  Map<String, dynamic> toJson() => {
    'type': type,
    'id': id,
    'name': name,
    'title': title,
    'description': description,
    'description_title': descriptionTitle,
    'group': group,
    'groupname': groupname,
    'group_expandable': groupExpandable,
    'group_expanded': groupExpanded,
    'state': state,
    'level': level,
    'url': url,
    'display_order': displayOrder,
  };
}

