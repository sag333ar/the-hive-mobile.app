import 'dart:convert';

import 'package:http/http.dart' as http;

enum HomeFeedStyle {
  trending,
  hot,
  newFeed,
}

class Communicator {
  static const hiveAuthServer = 'wss://hive-auth.arcange.eu';
  static const leoThreadServer = "https://alpha.leofinance.io";

  http.Request getRequestForFeed(
    String rpc,
    HomeFeedStyle type,
    String? startAuthor,
    String? startPermlink,
    String? tag,
  ) {
    var request = http.Request('POST', Uri.parse(rpc));
    request.body = json.encode({
      "id": 1,
      "jsonrpc": "2.0",
      "method": "bridge.get_ranked_posts",
      "params": {
        "sort": type == HomeFeedStyle.trending
            ? "trending"
            : type == HomeFeedStyle.hot
                ? "hot"
                : "created",
        "tag": tag ?? "",
        "observer": "hive.blog",
        // "limit": 100,
        "start_author": startAuthor,
        "start_permlink": startPermlink,
      }
    });
    return request;
  }

  http.Request getThreadSubjects() {
    var headers = {
      "Access-Control-Allow-Origin": "*",
    };
    var request = http.Request(
        'GET', Uri.parse('$leoThreadServer/threads?_data=routes%2Fthreads'));
    request.headers.addAll(headers);
    return request;
  }

  http.Request getRequestForTransferHistory(
    String rpc,
    int start,
    int end,
    String user,
  ) {
    var request = http.Request('POST', Uri.parse(rpc));
    request.body = json.encode({
      "id": 0,
      "jsonrpc": "2.0",
      "method": "condenser_api.get_account_history",
      "params": [user, start, end, "4", null]
    });
    return request;
  }

  http.Request getRequestForUpVotesOfPost(
    String rpc,
    String author,
    String permlink,
  ) {
    var request = http.Request('POST', Uri.parse(rpc));
    request.body = json.encode({
      "id": 10,
      "jsonrpc": "2.0",
      "method": "condenser_api.get_active_votes",
      "params": [author, permlink]
    });
    return request;
  }

  http.Request getRequestForCommentsOfPost(
    String rpc,
    String author,
    String permlink,
  ) {
    var request = http.Request('POST', Uri.parse(rpc));
    request.body = json.encode({
      "id": 0,
      "jsonrpc": "2.0",
      "method": "bridge.get_discussion",
      "params": {"author": author, "permlink": permlink}
    });
    return request;
  }

  http.Request getRequestForUser(
    String rpc,
    String user,
    String? startAuthor,
    String? startPermlink,
    String type,
  ) {
    var request = http.Request('POST', Uri.parse(rpc));
    request.body = json.encode({
      "id": 1,
      "jsonrpc": "2.0",
      "method": "bridge.get_account_posts",
      "params": {
        "sort": type,
        "account": user,
        "observer": "hive.blog",
        "start_author": startAuthor,
        "start_permlink": startPermlink,
      }
    });
    return request;
  }

  http.Request getRequestForCommunities(
    String rpc,
    String sort,
    String? query,
  ) {
    var request = http.Request('POST', Uri.parse(rpc));
    request.body = json.encode({
      "id": 7,
      "jsonrpc": "2.0",
      "method": "bridge.list_communities",
      "params": {
        "observer": "hive.blog",
        "query": query?.isEmpty == true ? null : query,
        "sort": sort,
      }
    });
    return request;
  }

  http.Request searchHiveUsers(
    String rpc,
    String query,
  ) {
    var request = http.Request('POST', Uri.parse(rpc));
    request.body = json.encode({
      "id": 12,
      "jsonrpc": "2.0",
      "method": "call",
      "params": [
        "condenser_api",
        "lookup_accounts",
        [query, 50]
      ]
    });
    return request;
  }

  http.Request getRequestForPost(String rpc, String author, String permlink) {
    var request = http.Request('POST', Uri.parse(rpc));
    request.body = json.encode({
      "id": 8,
      "jsonrpc": "2.0",
      "method": "bridge.get_discussion",
      "params": {
        "observer": "hive.blog",
        "author": author,
        "permlink": permlink
      }
    });
    return request;
  }

  http.Request getRequestForFollowersFollowings(
    String rpc,
    String author,
    String type,
  ) {
    var request = http.Request('POST', Uri.parse(rpc));
    var method = (type == "followers")
        ? "condenser_api.get_followers"
        : "condenser_api.get_following";
    request.body = json.encode({
      "id": 0,
      "jsonrpc": "2.0",
      "method": method,
      "params": [author, "", "blog", 1000]
    });
    return request;
  }

  http.Request getRequestForBlackListedMuted(
    String rpc,
    String author,
    String type,
  ) {
    var request = http.Request('POST', Uri.parse(rpc));
    var followType = (type == "blacklisted") ? "blacklisted" : "muted";
    request.body = json.encode({
      "id": 1,
      "jsonrpc": "2.0",
      "method": "bridge.get_follow_list",
      "params": {"observer": author, "follow_type": followType}
    });
    return request;
  }

  http.Request getRequestForUserInfo(String rpc, String author) {
    var request = http.Request('POST', Uri.parse(rpc));
    request.body = json.encode({
      "id": 3,
      "jsonrpc": "2.0",
      "method": "bridge.get_profile",
      "params": {"account": author, "observer": "hive.blog"}
    });
    return request;
  }

  http.Request getRequestForUserNotifications(String rpc, String author) {
    var request = http.Request('POST', Uri.parse(rpc));
    request.body = json.encode({
      "id": 9,
      "jsonrpc": "2.0",
      "method": "bridge.account_notifications",
      "params": {"account": author, "limit": 100}
    });
    return request;
  }

  http.Request getRequestForCommunityDetails(String rpc, String community) {
    var request = http.Request('POST', Uri.parse(rpc));
    request.body = json.encode({
      "id": 4,
      "jsonrpc": "2.0",
      "method": "bridge.get_community",
      "params": {"name": community, "observer": "hive.blog"}
    });
    return request;
  }

  http.Request getRequestForHiveBuzzBadges(String author) {
    var request = http.Request(
        'GET', Uri.parse('https://hivebuzz.me/api/badges/$author'));
    return request;
  }

  http.Request getRequestForPeakdBadges(String author) {
    var request = http.Request(
        'GET', Uri.parse('https://peakd.com/api/public/badge/$author'));
    return request;
  }

  getRequestForUserCommunitySubscriptions(String rpc, String author) {
    var request = http.Request('POST', Uri.parse(rpc));
    request.body = json.encode({
      "id": 18,
      "jsonrpc": "2.0",
      "method": "bridge.list_all_subscriptions",
      "params": {"account": author}
    });
    return request;
  }

  http.Request getRequestForUserAccount(String rpc, String author) {
    var request = http.Request('POST', Uri.parse(rpc));
    request.body = json.encode({
      "id": 25,
      "jsonrpc": "2.0",
      "method": "condenser_api.get_accounts",
      "params": [
        [author]
      ]
    });
    return request;
  }

  Future<String> getResponseString(http.Request request) async {
    http.StreamedResponse response = await request.send();
    if (response.statusCode == 200) {
      var string = await response.stream.bytesToString();
      return string;
    } else {
      throw response.reasonPhrase.toString();
    }
  }
}
