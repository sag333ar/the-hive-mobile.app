import 'dart:async';

import 'package:flutter/material.dart';

class HiveAuthData {
  String token;
  String expiry;
  String name;
  String authKey;
  HiveAuthData({
    required this.token,
    required this.expiry,
    required this.name,
    required this.authKey,
  });
}

class HiveBlocData {
  bool isDarkMode;
  String rpc;
  HiveAuthData? user;
  String? hiveKeychainUser;

  HiveBlocData({
    required this.isDarkMode,
    required this.rpc,
    required this.user,
    required this.hiveKeychainUser,
  });
}

class HiveBloc {
  final _controller = StreamController<HiveBlocData>();
  final themeColor = const Color.fromRGBO(228, 4, 36, 1.0);

  Stream<HiveBlocData> get appData {
    return _controller.stream;
  }

  void changeDomain(String rpc, HiveBlocData appData) {
    updateAppData(
      HiveBlocData(
        isDarkMode: appData.isDarkMode,
        rpc: rpc,
        user: appData.user,
        hiveKeychainUser: appData.hiveKeychainUser,
      ),
    );
  }

  void changeDarkMode(bool mode, HiveBlocData appData) {
    updateAppData(
      HiveBlocData(
        isDarkMode: mode,
        rpc: appData.rpc,
        user: appData.user,
        hiveKeychainUser: appData.hiveKeychainUser,
      ),
    );
  }

  void updateUserData(HiveAuthData? user, HiveBlocData appData) {
    updateAppData(
      HiveBlocData(
        isDarkMode: appData.isDarkMode,
        rpc: appData.rpc,
        user: user,
        hiveKeychainUser: null,
      ),
    );
  }

  void updateHKData(String? username, HiveBlocData appData) {
    updateAppData(
      HiveBlocData(
        isDarkMode: appData.isDarkMode,
        rpc: appData.rpc,
        user: null,
        hiveKeychainUser: username,
      ),
    );
  }

  void updateAppData(HiveBlocData appData) {
    _controller.sink.add(appData);
  }

  String userOwnerThumb(String value) {
    return "https://images.hive.blog/u/$value/avatar";
  }

  String resizedImage(String value) {
    return "https://images.hive.blog/320x160/$value";
  }
}

HiveBloc hiveBloc = HiveBloc();
