import 'package:flutter/material.dart';
import 'package:the_hive_app/my_app.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  runApp(const MyApp());
}
