import 'package:flutter/material.dart';
import 'package:the_hive_app/utilities/hive_bloc.dart';
import 'package:the_hive_app/utilities/models/feed.dart';
import 'package:the_hive_app/utilities/request_generator.dart';
import 'package:the_hive_app/widgets/error_state_widget.dart';
import 'package:the_hive_app/widgets/list_item.dart';
import 'package:the_hive_app/widgets/loading_state_widget.dart';

class FeedsScreen extends StatefulWidget {
  const FeedsScreen({
    Key? key,
    required this.feedType,
    required this.appData,
    required this.tag
  }) : super(key: key);
  final HomeFeedStyle feedType;
  final HiveBlocData appData;
  final String? tag;

  @override
  State<FeedsScreen> createState() => _FeedsScreenState();
}

class _FeedsScreenState extends State<FeedsScreen>
    with AutomaticKeepAliveClientMixin<FeedsScreen> {
  @override
  bool get wantKeepAlive => true;

  late Future<void> loadFeedData;
  var isLoadingNextPage = false;
  List<HivePostJsonItem> items = [];
  var isLoading = false;

  late ScrollController controller;

  @override
  void initState() {
    super.initState();
    loadFeedData = getFeedData();
    controller = ScrollController()..addListener(_scrollListener);
  }

  void _scrollListener() {
    if (controller.position.pixels == controller.position.maxScrollExtent) {
      getNextFeedData();
    }
  }

  Future<void> getFeedData() async {
    setState(() {
      isLoading = true;
      items = [];
    });
    var rpc = widget.appData.rpc;
    var request = Communicator().getRequestForFeed(
      rpc,
      widget.feedType,
      null,
      null,
      widget.tag,
    );
    var response = await Communicator().getResponseString(request);
    var responseData = FeedResponse.fromJsonString(response);
    setState(() {
      items = responseData.result;
      isLoading = false;
    });
  }

  Future<void> getNextFeedData() async {
    setState(() {
      isLoadingNextPage = true;
    });
    var rpc = widget.appData.rpc;
    var request = Communicator().getRequestForFeed(
      rpc,
      widget.feedType,
      items.last.author,
      items.last.permlink,
      widget.tag,
    );
    var response = await Communicator().getResponseString(request);
    var responseData = FeedResponse.fromJsonString(response);
    setState(() {
      items += responseData.result;
      isLoadingNextPage = false;
    });
  }

  Widget _listView() {
    return RefreshIndicator(
      onRefresh: () {
        return getFeedData();
      },
      child: ListView.separated(
        itemBuilder: (c, i) {
          return ListItemWidget(
            item: items[i],
            isUserChannel: false,
            shouldShowPostImage: true,
            onUserProfileTapped: () {
              var routeName = '/@${items[i].author}';
              Navigator.of(context).pushNamed(routeName);
            },
            onPostTapped: () {
              var routeName = '/@${items[i].author}/${items[i].permlink}';
              Navigator.of(context).pushNamed(routeName);
            },
          );
        },
        separatorBuilder: (c, i) => const Divider(
          color: Colors.transparent,
          height: 1,
        ),
        itemCount: items.length,
        controller: controller,
      ),
    );
  }

  Widget _displayData() {
    if (items.isEmpty) {
      return const Center(
        child: Text('No data found.\nPlease tap ⚡️ button & change filters.'),
      );
    }
    return _listView();
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return FutureBuilder(
      future: loadFeedData,
      builder: (builder, snapshot) {
        if (snapshot.hasError) {
          return ErrorStateWidget(onRetry: () {
            setState(() {
              loadFeedData = getFeedData();
            });
          });
        } else if (snapshot.connectionState == ConnectionState.done) {
          return _displayData();
        } else {
          return const LoadingStateWidget();
        }
      },
    );
  }
}
