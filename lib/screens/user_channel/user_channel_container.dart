import 'package:flutter/material.dart';
import 'package:the_hive_app/screens/user_channel/user_blog_comments.dart';
import 'package:the_hive_app/screens/user_channel/user_blog_screen.dart';
import 'package:the_hive_app/screens/user_channel/user_followers.dart';
import 'package:the_hive_app/screens/user_channel/user_info_screen.dart';
import 'package:the_hive_app/screens/user_channel/user_notifications.dart';
import 'package:the_hive_app/screens/user_channel/user_wallet.dart';
import 'package:the_hive_app/utilities/hive_bloc.dart';
import 'package:the_hive_app/widgets/custom_circle_avatar.dart';
import 'package:share_plus_dialog/share_plus_dialog.dart';

class UserChannelContainerScreen extends StatefulWidget {
  const UserChannelContainerScreen({
    Key? key,
    required this.username,
    required this.appData,
  }) : super(key: key);
  final String username;
  final HiveBlocData appData;

  @override
  State<UserChannelContainerScreen> createState() =>
      _UserChannelContainerScreenState();
}

class _UserChannelContainerScreenState extends State<UserChannelContainerScreen>
    with TickerProviderStateMixin {
  late int selectedTabIndex;
  late TabController controller;
  final List<String> textForTabs = [
    'Blog & Posts',
    'Comments & Replies',
    'Followers, Followings, Muted, Blacklists',
    'Info',
    'Wallet',
  ];

  final List<IconData> iconsForTabs = [
    Icons.newspaper,
    Icons.comment,
    Icons.person_add_alt,
    Icons.info,
    Icons.wallet,
    Icons.notifications_active
  ];

  @override
  void initState() {
    super.initState();
    selectedTabIndex = 0;
    controller = TabController(vsync: this, length: _tabs().length);
    controller.addListener(() {
      setState(() {
        selectedTabIndex = controller.index;
      });
    });
  }

  String _titleText() {
    return textForTabs[selectedTabIndex];
  }

  List<Tab> _tabs() {
    return iconsForTabs.map((e) => Tab(icon: Icon(e))).toList();
  }

  List<Text> _tabTextWidgets() {
    return textForTabs.map((e) => Text(e)).toList();
  }

  List<Widget> _tabViews(HiveBlocData data) {
    return [
      UserBlogAndPostsScreen(
        username: widget.username,
        appData: widget.appData,
      ),
      UserBlogCommentsAndReplies(
        username: widget.username,
        appData: widget.appData,
      ),
      UserFollowerFollowingsBM(
        username: widget.username,
        appData: widget.appData,
      ),
      UserInfoScreen(
        appData: widget.appData,
        username: widget.username,
      ),
      UserWalletScreen(
        appData: widget.appData,
        username: widget.username,
      ),
      UserNotificationsScreen(
        appData: widget.appData,
        username: widget.username,
      )
    ];
  }

  @override
  Widget build(BuildContext context) {
    var count = _tabs().length;
    return DefaultTabController(
      length: count,
      initialIndex: selectedTabIndex,
      child: Builder(
        builder: (context) {
          return Scaffold(
            appBar: AppBar(
              centerTitle: false,
              title: ListTile(
                leading: CustomCircleAvatar(
                  url: hiveBloc.userOwnerThumb(widget.username),
                  width: 44,
                  height: 44,
                ),
                title: Text(widget.username),
                subtitle: Text(_titleText()),
              ),
              actions: [
                IconButton(
                  icon: const Icon(Icons.share),
                  onPressed: () {
                    ShareDialog.share(context,
                        'Hey! Check out this user https://the-hive-mobile.app/@${widget.username}',
                        platforms: SharePlatform.defaults, isUrl: true);
                  },
                ),
              ],
              bottom: TabBar(
                tabs: _tabs(),
                controller: controller,
              ),
            ),
            body: SafeArea(
              child: TabBarView(
                controller: controller,
                children: _tabViews(widget.appData),
              ),
            ),
          );
        },
      ),
    );
  }
}
