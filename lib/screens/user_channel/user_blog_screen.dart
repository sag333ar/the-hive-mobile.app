import 'package:flutter/material.dart';
import 'package:the_hive_app/utilities/hive_bloc.dart';
import 'package:the_hive_app/utilities/models/feed.dart';
import 'package:the_hive_app/utilities/request_generator.dart';
import 'package:the_hive_app/widgets/error_state_widget.dart';
import 'package:the_hive_app/widgets/fab_custom.dart';
import 'package:the_hive_app/widgets/fab_overlay.dart';
import 'package:the_hive_app/widgets/list_item.dart';
import 'package:the_hive_app/widgets/loading_state_widget.dart';

class UserBlogAndPostsScreen extends StatefulWidget {
  const UserBlogAndPostsScreen({Key? key, required this.username,
    required this.appData,
  }) : super(key: key);
  final String username;
  final HiveBlocData appData;

  @override
  State<UserBlogAndPostsScreen> createState() => _UserBlogAndPostsScreenState();
}

class _UserBlogAndPostsScreenState extends State<UserBlogAndPostsScreen> {
  var isFilterMenuOn = false;
  var showingPosts = false;
  var key = GlobalKey();

  List<FabOverItemData> _fabItems() {
    List<FabOverItemData> fabItems = [];
    fabItems.add(
      FabOverItemData(
        displayName: 'Posts',
        icon: Icons.note_alt,
        onTap: () {
          setState(() {
            isFilterMenuOn = false;
            showingPosts = true;
            key = GlobalKey();
          });
        },
      ),
    );
    fabItems.add(
      FabOverItemData(
        displayName: 'Blog',
        icon: Icons.newspaper,
        onTap: () {
          setState(() {
            isFilterMenuOn = false;
            showingPosts = false;
            key = GlobalKey();
          });
        },
      ),
    );
    fabItems.add(
      FabOverItemData(
        displayName: 'Close',
        icon: Icons.close,
        onTap: () {
          setState(() {
            isFilterMenuOn = false;
          });
        },
      ),
    );
    return fabItems;
  }

  Widget _fabContainer() {
    if (!isFilterMenuOn) {
      return FabCustom(
        icon: Icons.bolt,
        onTap: () {
          setState(() {
            isFilterMenuOn = true;
          });
        },
      );
    }
    return FabOverlay(
      items: _fabItems(),
      onBackgroundTap: () {
        setState(() {
          isFilterMenuOn = false;
        });
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(child: Stack(
      children: [
        UserBlogScreen(
          username: widget.username,
          appData: widget.appData,
          isPosts: showingPosts,
          key: key,
        ),
        _fabContainer(),
      ],
    ));
  }
}


class UserBlogScreen extends StatefulWidget {
  const UserBlogScreen({
    Key? key,
    required this.username,
    required this.appData,
    required this.isPosts,
  }) : super(key: key);
  final String username;
  final HiveBlocData appData;
  final bool isPosts;

  @override
  State<UserBlogScreen> createState() => _UserBlogScreenState();
}

class _UserBlogScreenState extends State<UserBlogScreen> {
  late Future<void> loadFeedData;
  var isLoadingNextPage = false;
  List<HivePostJsonItem> items = [];
  var isLoading = false;

  late ScrollController controller;

  @override
  void initState() {
    super.initState();
    loadFeedData = getFeedData();
    controller = ScrollController()..addListener(_scrollListener);
  }

  void _scrollListener() {
    if (controller.position.pixels == controller.position.maxScrollExtent) {
      getNextFeedData();
    }
  }

  Future<void> getFeedData() async {
    setState(() {
      isLoading = true;
      items = [];
    });
    var rpc = widget.appData.rpc;
    var request = Communicator().getRequestForUser(
      rpc,
      widget.username,
      null,
      null,
      widget.isPosts ? "posts" : "blog",
    );
    var response = await Communicator().getResponseString(request);
    var responseData = FeedResponse.fromJsonString(response);
    setState(() {
      items = responseData.result;
      isLoading = false;
    });
  }

  Future<void> getNextFeedData() async {
    setState(() {
      isLoadingNextPage = true;
    });
    var rpc = widget.appData.rpc;
    var request = Communicator().getRequestForUser(
      rpc,
      widget.username,
      items.last.author,
      items.last.permlink,
      widget.isPosts ? "posts" : "blog",
    );
    var response = await Communicator().getResponseString(request);
    var responseData = FeedResponse.fromJsonString(response);
    setState(() {
      items += responseData.result;
      isLoadingNextPage = false;
    });
  }

  Widget _listView() {
    return RefreshIndicator(
      onRefresh: () {
        return getFeedData();
      },
      child: ListView.separated(
        itemBuilder: (c, i) {
          return ListItemWidget(
            item: items[i],
            isUserChannel: true,
            shouldShowPostImage: true,
            onUserProfileTapped: () {},
            onPostTapped: () {
              var routeName = '/@${items[i].author}/${items[i].permlink}';
              Navigator.of(context).pushNamed(routeName);
            },
          );
        },
        separatorBuilder: (c, i) => const Divider(
          color: Colors.transparent,
          height: 1,
        ),
        itemCount: items.length,
        controller: controller,
      ),
    );
  }

  Widget _displayData() {
    if (items.isEmpty) {
      return const Center(
        child: Text('No data found'),
      );
    }
    return _listView();
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: loadFeedData,
      builder: (builder, snapshot) {
        if (snapshot.hasError) {
          return ErrorStateWidget(onRetry: () {
            setState(() {
              loadFeedData = getFeedData();
            });
          });
        } else if (snapshot.connectionState == ConnectionState.done) {
          return _displayData();
        } else {
          return const LoadingStateWidget();
        }
      },
    );
  }
}
