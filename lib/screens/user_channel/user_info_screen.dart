import 'package:flutter/material.dart';
import 'package:the_hive_app/utilities/hive_bloc.dart';
import 'package:the_hive_app/utilities/models/hive_buzz_badges.dart';
import 'package:the_hive_app/utilities/models/peakd_badges.dart';
import 'package:the_hive_app/utilities/models/user_community_subscriptions.dart';
import 'package:the_hive_app/utilities/models/user_info.dart';
import 'package:the_hive_app/utilities/request_generator.dart';
import 'package:the_hive_app/widgets/custom_circle_avatar.dart';
import 'package:the_hive_app/widgets/loading_state_widget.dart';

class UserInfoScreen extends StatefulWidget {
  const UserInfoScreen({
    Key? key,
    required this.appData,
    required this.username,
  }) : super(key: key);
  final HiveBlocData appData;
  final String username;

  @override
  State<UserInfoScreen> createState() => _UserInfoScreenState();
}

class _UserInfoScreenState extends State<UserInfoScreen> {
  var isLoading = true;
  UserInfoResponseModel? response;
  List<List<String>> communitySubscriptions = [];
  List<HiveBuzzBadgesItem> hiveBuzzBadges = [];
  List<PeakdBadgesItem> peakdBadgs = [];

  @override
  void initState() {
    super.initState();
    getFeedData();
    getHiveBuzzBadges();
    getPeakdBadges();
  }

  List<Widget> userInfoWidgets() {
    return [
      Stack(
        children: [
          Image.network(
            response!.result.getCoverImage(),
            height: 160,
            width: double.infinity,
            fit: BoxFit.fill,
            errorBuilder: (c, o, s) {
              return Image.network(
                'https://images.hive.blog/0x0/https://files.peakd.com/file/peakd-hive/sagarkothari88/sagar.kothari.png',
                height: 160,
                width: double.infinity,
                fit: BoxFit.fill,
              );
            },
          ),
          Center(
            child: Container(
              margin: const EdgeInsets.only(top: 5),
              child: CustomCircleAvatar(
                height: 150,
                width: 150,
                url: response!.result.metadata.profile.profileImage,
              ),
            ),
          ),
        ],
      ),
      ListTile(
        title: Text('@${widget.username}', textAlign: TextAlign.center),
        subtitle: Text(
            '${response!.result.metadata.profile.name}\n${response!.result.metadata.profile.about}',
            textAlign: TextAlign.center),
      ),
      ListTile(
        leading: const Icon(Icons.post_add, color: Colors.white),
        title: const Text('Posts'),
        trailing: Text('${response!.result.postCount}'),
      ),
      ListTile(
        leading: const Icon(Icons.person_add, color: Colors.white),
        title: const Text('Followers'),
        trailing: Text('${response!.result.stats.followers}'),
      ),
      ListTile(
        leading: const Icon(Icons.person_add_alt, color: Colors.white),
        title: const Text('Followings'),
        trailing: Text('${response!.result.stats.following}'),
      ),
      ListTile(
        leading: const Icon(Icons.trending_up, color: Colors.white),
        title: const Text('Reputation'),
        trailing: Text(
            '${response!.result.reputation.toDouble().toStringAsFixed(2)} %'),
      ),
      ListTile(
        leading: const Icon(Icons.public, color: Colors.white),
        title: const Text('Website'),
        trailing: Text(response!.result.metadata.profile.website),
      ),
      ListTile(
        leading: const Icon(Icons.location_on, color: Colors.white),
        title: const Text('Location'),
        trailing: Text(response!.result.metadata.profile.location),
      ),
      ListTile(
        leading: const Icon(Icons.remove_red_eye, color: Colors.white),
        title: const Text('Last Active'),
        trailing: Text(response!.result.activeAgo()),
      ),
      ListTile(
        leading: const Icon(Icons.login, color: Colors.white),
        title: const Text('Joined Hive'),
        trailing: Text(response!.result.joinedAgo()),
      ),
    ];
  }

  // Community Subscription Widgets
  List<Widget> comSubWidgets() {
    if (communitySubscriptions.isEmpty) {
      return [
        const ListTile(
          title: Text('Community Subscriptions'),
          trailing: Text('None'),
        ),
      ];
    }
    return [
          const ListTile(
            title: Text(
              '- Community Subscriptions - ',
              textAlign: TextAlign.center,
            ),
          )
        ] +
        communitySubscriptions.map((element) {
          return ListTile(
            leading: CustomCircleAvatar(
              height: 44,
              width: 44,
              url: hiveBloc.userOwnerThumb(element[0]),
            ),
            title: Text(element[1]),
            trailing: Text(element[2]),
          );
        }).toList();
  }

  List<Widget> hiveBuzzBadgesWidgets() {
    if (hiveBuzzBadges.isEmpty) {
      return [];
    }
    return [
      const ListTile(
        title: Text(
          '- HiveBuzz Badges -',
          textAlign: TextAlign.center,
        ),
      )
    ] + hiveBuzzBadges.map((element) {
      return ListTile(
        leading: CustomCircleAvatar(
          height: 44,
          width: 44,
          url: element.url,
        ),
        title: Text(element.title),
      );
    }).toList();
  }

  List<Widget> peakdBadgesWidgets() {
    if (peakdBadgs.isEmpty) {
      return [];
    }
    return [
      const ListTile(
        title: Text(
          '- Peakd Badges -',
          textAlign: TextAlign.center,
        ),
      )
    ] + peakdBadgs.map((element) {
      return ListTile(
        leading: CustomCircleAvatar(
          height: 44,
          width: 44,
          url: 'https://images.hive.blog/u/${element.name}/avatar/medium',
        ),
        title: Text(element.title),
      );
    }).toList();
  }

  @override
  Widget build(BuildContext context) {
    if (isLoading) {
      return const LoadingStateWidget();
    } else if (response != null) {
      return SingleChildScrollView(
        child: Column(
          children: userInfoWidgets() + comSubWidgets() + hiveBuzzBadgesWidgets() + peakdBadgesWidgets(),
        ),
      );
    } else {
      return const LoadingStateWidget();
    }
  }

  Future<void> getHiveBuzzBadges() async {
    var request = Communicator().getRequestForHiveBuzzBadges(
      widget.username,
    );
    var resp = await Communicator().getResponseString(request);
    var items = HiveBuzzBadgesItem.fromJsonString(resp);
    var filteredItems = items.where((element) => element.level != 5000).toList();
    setState(() {
      hiveBuzzBadges = filteredItems;
    });
  }

  Future<void> getPeakdBadges() async {
    var request = Communicator().getRequestForPeakdBadges(
      widget.username,
    );
    var resp = await Communicator().getResponseString(request);
    var items = PeakdBadgesItem.fromJsonString(resp);
    setState(() {
      peakdBadgs = items;
    });
  }

  Future<void> getFeedData() async {
    setState(() {
      isLoading = true;
      response = null;
    });
    var rpc = widget.appData.rpc;
    var request = Communicator().getRequestForUserInfo(
      rpc,
      widget.username,
    );
    var resp = await Communicator().getResponseString(request);
    var responseData = UserInfoResponseModel.fromJsonString(resp);
    var comSubRq = Communicator().getRequestForUserCommunitySubscriptions(
      rpc,
      widget.username,
    );
    var comSubResp = await Communicator().getResponseString(comSubRq);
    var data = UserCommunitySubscriptions.fromJsonString(comSubResp);
    setState(() {
      response = responseData;
      communitySubscriptions = data.result;
      isLoading = false;
    });
  }
}
