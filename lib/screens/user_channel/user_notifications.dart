import 'package:flutter/material.dart';
import 'package:the_hive_app/utilities/hive_bloc.dart';
import 'package:the_hive_app/utilities/models/user_notifications.dart';
import 'package:the_hive_app/utilities/request_generator.dart';
import 'package:the_hive_app/widgets/custom_circle_avatar.dart';
import 'package:the_hive_app/widgets/loading_state_widget.dart';
import 'package:timeago/timeago.dart' as timeago;

class UserNotificationsScreen extends StatefulWidget {
  const UserNotificationsScreen({
    Key? key,
    required this.username,
    required this.appData,
  }) : super(key: key);
  final String username;
  final HiveBlocData appData;

  @override
  State<UserNotificationsScreen> createState() =>
      _UserNotificationsScreenState();
}

class _UserNotificationsScreenState extends State<UserNotificationsScreen>
    with AutomaticKeepAliveClientMixin<UserNotificationsScreen> {

  @override
  bool get wantKeepAlive => true;

  late Future<void> loadUserNotification;
  List<UserNotificationItem> items = [];
  var isLoading = false;

  @override
  void initState() {
    super.initState();
    loadUserNotification = getUserNotification();
  }

  Future<void> getUserNotification() async {
    setState(() {
      isLoading = true;
      items = [];
    });
    var rpc = widget.appData.rpc;
    var request = Communicator().getRequestForUserNotifications(
      rpc,
      widget.username,
    );
    var response = await Communicator().getResponseString(request);
    var responseData = UserNotifications.fromJsonString(response);
    setState(() {
      items = responseData.result;
      isLoading = false;
    });
  }

  Widget _listView() {
    return RefreshIndicator(
      onRefresh: () {
        return getUserNotification();
      },
      child: ListView.separated(
        itemBuilder: (c, i) {
          var date = DateTime.tryParse(items[i].date) ?? DateTime.now();
          var timeAgo = timeago.format(date);
          var user = items[i].msg.split(" ")[0].replaceAll("@", "");
          return ListTile(
            leading: CustomCircleAvatar(
              url: hiveBloc.userOwnerThumb(user),
              width: 44,
              height: 44,
            ),
            title: Text(items[i].msg),
            subtitle: Text(timeAgo),
          );
        },
        separatorBuilder: (c, i) => const Divider(
          color: Colors.transparent,
          height: 1,
        ),
        itemCount: items.length,
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    if (isLoading) {
      return const LoadingStateWidget();
    } else {
      return _listView();
    }
  }
}
