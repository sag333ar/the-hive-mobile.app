import 'package:flutter/material.dart';
import 'package:the_hive_app/utilities/hive_bloc.dart';
import 'package:the_hive_app/utilities/models/account_details.dart';
import 'package:the_hive_app/utilities/request_generator.dart';
import 'package:the_hive_app/widgets/loading_state_widget.dart';

class UserWalletScreen extends StatefulWidget {
  const UserWalletScreen({
    Key? key,
    required this.appData,
    required this.username,
  }) : super(key: key);
  final HiveBlocData appData;
  final String username;

  @override
  State<UserWalletScreen> createState() => _UserWalletScreenState();
}

class _UserWalletScreenState extends State<UserWalletScreen> {
  var isLoading = true;
  AccountDetailsResponse? response;

  @override
  void initState() {
    super.initState();
    getAccountData();
  }

  Future<void> getAccountData() async {
    setState(() {
      isLoading = true;
      response = null;
    });
    var rpc = widget.appData.rpc;
    var request = Communicator().getRequestForUserAccount(
      rpc,
      widget.username,
    );
    var resp = await Communicator().getResponseString(request);
    var responseData = AccountDetailsResponse.fromJsonString(resp);
    setState(() {
      response = responseData;
      isLoading = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    if (isLoading) {
      return const LoadingStateWidget();
    } else if (response != null) {
      return ListView(
        children: [
          ListTile(
            title: const Text('Hive'),
            trailing: Text(response!.result[0].balance),
          ),
          ListTile(
            title: const Text('Hive Savings'),
            trailing: Text(response!.result[0].savingsBalance),
          ),
          ListTile(
            title: const Text('HBD'),
            trailing: Text(response!.result[0].hbdBalance),
          ),
          ListTile(
            title: const Text('HBD Savings'),
            trailing: Text(response!.result[0].savingsHbdBalance),
          )
        ],
      );
    } else {
      return const LoadingStateWidget();
    }
  }
}
