import 'package:flutter/material.dart';
import 'package:the_hive_app/utilities/hive_bloc.dart';
import 'package:the_hive_app/utilities/models/user_follower_response.dart';
import 'package:the_hive_app/utilities/request_generator.dart';
import 'package:the_hive_app/widgets/custom_circle_avatar.dart';
import 'package:the_hive_app/widgets/error_state_widget.dart';
import 'package:the_hive_app/widgets/fab_custom.dart';
import 'package:the_hive_app/widgets/fab_overlay.dart';
import 'package:the_hive_app/widgets/loading_state_widget.dart';

class UserFollowerFollowingsBM extends StatefulWidget {
  const UserFollowerFollowingsBM({
    Key? key,
    required this.username,
    required this.appData,
  }) : super(key: key);
  final String username;
  final HiveBlocData appData;

  @override
  State<UserFollowerFollowingsBM> createState() =>
      _UserFollowerFollowingsBMState();
}

class _UserFollowerFollowingsBMState extends State<UserFollowerFollowingsBM> {
  var isFilterMenuOn = false;
  var type = 'followers';
  var key = GlobalKey();

  List<FabOverItemData> _fabItems() {
    return [
      FabOverItemData(
        displayName: 'Followers',
        icon: Icons.person_add,
        onTap: () {
          setState(() {
            isFilterMenuOn = false;
            type = 'followers';
            key = GlobalKey();
          });
        },
      ),
      FabOverItemData(
        displayName: 'Followings',
        icon: Icons.person_add_alt,
        onTap: () {
          setState(() {
            isFilterMenuOn = false;
            type = 'followings';
            key = GlobalKey();
          });
        },
      ),
      FabOverItemData(
        displayName: 'Blacklisted',
        icon: Icons.block,
        onTap: () {
          setState(() {
            isFilterMenuOn = false;
            type = 'blacklisted';
            key = GlobalKey();
          });
        },
      ),
      FabOverItemData(
        displayName: 'Muted',
        icon: Icons.speaker_notes_off,
        onTap: () {
          setState(() {
            isFilterMenuOn = false;
            type = 'muted';
            key = GlobalKey();
          });
        },
      ),
      FabOverItemData(
        displayName: 'Close',
        icon: Icons.close,
        onTap: () {
          setState(() {
            isFilterMenuOn = false;
          });
        },
      ),
    ];
  }

  Widget _fabContainer() {
    if (!isFilterMenuOn) {
      return FabCustom(
        icon: Icons.bolt,
        onTap: () {
          setState(() {
            isFilterMenuOn = true;
          });
        },
      );
    }
    return FabOverlay(
      items: _fabItems(),
      onBackgroundTap: () {
        setState(() {
          isFilterMenuOn = false;
        });
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(child: Stack(
      children: [
        UserFollowersScreen(
          username: widget.username,
          appData: widget.appData,
          type: type,
          key: key,
        ),
        _fabContainer(),
      ],
    ));
  }
}

class UserFollowersScreen extends StatefulWidget {
  const UserFollowersScreen({
    Key? key,
    required this.username,
    required this.appData,
    required this.type,
  }) : super(key: key);
  final String username;
  final HiveBlocData appData;
  final String type;

  @override
  State<UserFollowersScreen> createState() => _UserFollowersScreenState();
}

class _UserFollowersScreenState extends State<UserFollowersScreen> {
  late Future<List<UserFollowersResponseItem>> loadData;

  @override
  void initState() {
    super.initState();
    loadData = getFollowers();
  }

  Future<List<UserFollowersResponseItem>> getFollowers() async {
    var rpc = widget.appData.rpc;
    var request = (widget.type == 'followers' || widget.type == 'followings')
        ? Communicator().getRequestForFollowersFollowings(
            rpc,
            widget.username,
            widget.type,
          )
        : Communicator().getRequestForBlackListedMuted(
            rpc,
            widget.username,
            widget.type,
          );
    var resp = await Communicator().getResponseString(request);
    var responseData = UserFollowersResponse.fromJsonString(resp);
    return responseData.result;
  }

  Widget _container(List<UserFollowersResponseItem> data) {
    if (data.isEmpty) {
      return Center(child: Text('No ${widget.type} found.'));
    }
    return SafeArea(
      child: ListView.separated(
        itemBuilder: (c, i) {
          var name = widget.type == 'followers'
              ? data[i].follower ?? ''
              : widget.type == 'followings'
                  ? data[i].following ?? ''
                  : data[i].name ?? '';
          return ListTile(
            leading: CustomCircleAvatar(
              url: hiveBloc.userOwnerThumb(name),
              width: 44,
              height: 44,
            ),
            title: Text(name),
            onTap: () {
              Navigator.of(context).pushNamed('/@$name');
            },
          );
        },
        separatorBuilder: (c, i) => const Divider(),
        itemCount: data.length,
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Container(
          margin: const EdgeInsets.only(top: 30),
          child: FutureBuilder(
            future: loadData,
            builder: (builder, snapshot) {
              if (snapshot.hasError) {
                return ErrorStateWidget(onRetry: () {
                  setState(() {
                    loadData = getFollowers();
                  });
                });
              } else if (snapshot.connectionState == ConnectionState.done) {
                var data = snapshot.data as List<UserFollowersResponseItem>;
                return _container(data);
              } else {
                return const LoadingStateWidget();
              }
            },
          ),
        ),
        Container(
          margin: const EdgeInsets.only(top: 10),
          child: Row(
            children: [
              const Spacer(),
              Text('Showing ${widget.type}'),
              const Spacer(),
            ],
          ),
        )
      ],
    );
  }
}
