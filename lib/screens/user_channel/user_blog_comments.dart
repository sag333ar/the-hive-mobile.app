import 'package:flutter/material.dart';
import 'package:the_hive_app/utilities/hive_bloc.dart';
import 'package:the_hive_app/utilities/models/feed.dart';
import 'package:the_hive_app/utilities/request_generator.dart';
import 'package:the_hive_app/widgets/error_state_widget.dart';
import 'package:the_hive_app/widgets/fab_custom.dart';
import 'package:the_hive_app/widgets/fab_overlay.dart';
import 'package:the_hive_app/widgets/list_item.dart';
import 'package:the_hive_app/widgets/loading_state_widget.dart';

class UserBlogCommentsAndReplies extends StatefulWidget {
  const UserBlogCommentsAndReplies({Key? key,
    required this.username,
    required this.appData,
  }) : super(key: key);
  final String username;
  final HiveBlocData appData;

  @override
  State<UserBlogCommentsAndReplies> createState() => _UserBlogCommentsAndRepliesState();
}

class _UserBlogCommentsAndRepliesState extends State<UserBlogCommentsAndReplies> {
  var isFilterMenuOn = false;
  var showingComments = true;
  var key = GlobalKey();

  List<FabOverItemData> _fabItems() {
    List<FabOverItemData> fabItems = [];
    fabItems.add(
      FabOverItemData(
        displayName: 'Comments',
        icon: Icons.comment,
        onTap: () {
          setState(() {
            isFilterMenuOn = false;
            showingComments = true;
            key = GlobalKey();
          });
        },
      ),
    );
    fabItems.add(
      FabOverItemData(
        displayName: 'Replies',
        icon: Icons.reply,
        onTap: () {
          setState(() {
            isFilterMenuOn = false;
            showingComments = false;
            key = GlobalKey();
          });
        },
      ),
    );
    fabItems.add(
      FabOverItemData(
        displayName: 'Close',
        icon: Icons.close,
        onTap: () {
          setState(() {
            isFilterMenuOn = false;
          });
        },
      ),
    );
    return fabItems;
  }

  Widget _fabContainer() {
    if (!isFilterMenuOn) {
      return FabCustom(
        icon: Icons.bolt,
        onTap: () {
          setState(() {
            isFilterMenuOn = true;
          });
        },
      );
    }
    return FabOverlay(
      items: _fabItems(),
      onBackgroundTap: () {
        setState(() {
          isFilterMenuOn = false;
        });
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(child: Stack(
      children: [
        UserBlogComments(
          username: widget.username,
          appData: widget.appData,
          isComments: showingComments,
          key: key,
        ),
        _fabContainer(),
      ],
    ));
  }
}


class UserBlogComments extends StatefulWidget {
  const UserBlogComments({
    Key? key,
    required this.username,
    required this.appData,
    required this.isComments,
  }) : super(key: key);
  final String username;
  final HiveBlocData appData;
  final bool isComments;

  @override
  State<UserBlogComments> createState() => _UserBlogCommentsState();
}

class _UserBlogCommentsState extends State<UserBlogComments> {
  late Future<void> loadFeedData;
  var isLoadingNextPage = false;
  List<HivePostJsonItem> items = [];
  var isLoading = false;

  late ScrollController controller;

  @override
  void initState() {
    super.initState();
    loadFeedData = getFeedData();
    controller = ScrollController()..addListener(_scrollListener);
  }

  void _scrollListener() {
    if (controller.position.pixels == controller.position.maxScrollExtent) {
      getNextFeedData();
    }
  }

  Future<void> getFeedData() async {
    setState(() {
      isLoading = true;
      items = [];
    });
    var rpc = widget.appData.rpc;
    var request = Communicator().getRequestForUser(
      rpc,
      widget.username,
      null,
      null,
      widget.isComments ? "comments" : "replies",
    );
    var response = await Communicator().getResponseString(request);
    var responseData = FeedResponse.fromJsonString(response);
    setState(() {
      items = responseData.result;
      isLoading = false;
    });
  }

  Future<void> getNextFeedData() async {
    setState(() {
      isLoadingNextPage = true;
    });
    var rpc = widget.appData.rpc;
    var request = Communicator().getRequestForUser(
      rpc,
      widget.username,
      items.last.author,
      items.last.permlink,
      widget.isComments ? "comments" : "replies",
    );
    var response = await Communicator().getResponseString(request);
    var responseData = FeedResponse.fromJsonString(response);
    setState(() {
      items += responseData.result;
      isLoadingNextPage = false;
    });
  }

  Widget _listView() {
    return RefreshIndicator(
      onRefresh: () {
        return getFeedData();
      },
      child: ListView.separated(
        itemBuilder: (c, i) {
          return ListItemWidget(
            item: items[i],
            isUserChannel: true,
            shouldShowPostImage: false,
            onUserProfileTapped: () {},
            onPostTapped: () {
              var routeName = '/@${items[i].author}/${items[i].permlink}';
              Navigator.of(context).pushNamed(routeName);
            },
          );
        },
        separatorBuilder: (c, i) => const Divider(
          color: Colors.transparent,
          height: 1,
        ),
        itemCount: items.length,
        controller: controller,
      ),
    );
  }

  Widget _displayData() {
    if (items.isEmpty) {
      return const Center(
        child: Text('No data found.\nPlease tap ⚡️ button & change filters.'),
      );
    }
    return _listView();
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: loadFeedData,
      builder: (builder, snapshot) {
        if (snapshot.hasError) {
          return ErrorStateWidget(onRetry: () {
            setState(() {
              loadFeedData = getFeedData();
            });
          });
        } else if (snapshot.connectionState == ConnectionState.done) {
          return _displayData();
        } else {
          return const LoadingStateWidget();
        }
      },
    );
  }
}
