import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:scroll_navigation/scroll_navigation.dart';
import 'package:the_hive_app/utilities/hive_bloc.dart';
import 'package:the_hive_app/utilities/models/threads/thread_names.dart';
import 'package:the_hive_app/utilities/request_generator.dart';
import 'package:the_hive_app/widgets/error_state_widget.dart';
import 'package:the_hive_app/widgets/loading_state_widget.dart';

class ThreadsContainer extends StatefulWidget {
  const ThreadsContainer({Key? key}) : super(key: key);

  @override
  State<ThreadsContainer> createState() => _ThreadsContainerState();
}

class _ThreadsContainerState extends State<ThreadsContainer> {
  late Future<ThreadNamesResponse> _future;

  @override
  void initState() {
    super.initState();
    _future = getThreadData();
  }

  Future<ThreadNamesResponse> getThreadData() async {
    var request = Communicator().getThreadSubjects();
    try {
      var response = await Communicator().getResponseString(request);
      var responseData = ThreadNamesResponse.fromJsonString(response);
      return responseData;
    } catch(e) {
      log('Error is -${e.toString()}');
      rethrow;
    }
  }

  Widget _container(ThreadNamesResponse data) {
    var pages = data.tags.map((e) {
      return Center(child: Text(e.name));
    }).toList();
    var items = data.tags.map((e) {
      return ScrollNavigationItem(title: e.name);
    }).toList();
    return ScrollNavigation(
      bodyStyle: NavigationBodyStyle(
        background: hiveBloc.themeColor,
        borderRadius: const BorderRadius.vertical(bottom: Radius.circular(20)),
      ),
      barStyle: NavigationBarStyle(
        background: hiveBloc.themeColor,
        elevation: 0.0,
        activeColor: Colors.white
      ),
      pages: pages,
      items: items,
    );
  }

  Widget _bodyFuture() {
    return FutureBuilder(
      future: _future,
      builder: (builder, snapshot) {
        if (snapshot.hasError) {
          return ErrorStateWidget(onRetry: (){
            setState(() {
              _future = getThreadData();
            });
          });
        } else if (snapshot.connectionState == ConnectionState.done) {
          var data = snapshot.data as ThreadNamesResponse;
          return _container(data);
        } else {
          return const LoadingStateWidget();
        }
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Hive ♦️ Threads 🪡 by Leo 🦁"),
      ),
      body: _bodyFuture(),
    );
  }


}
