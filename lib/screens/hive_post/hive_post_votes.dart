import 'package:flutter/material.dart';
import 'package:the_hive_app/utilities/hive_bloc.dart';
import 'package:the_hive_app/utilities/models/feed.dart';
import 'package:the_hive_app/utilities/models/post_voters.dart';
import 'package:the_hive_app/utilities/request_generator.dart';
import 'package:the_hive_app/widgets/custom_circle_avatar.dart';
import 'package:the_hive_app/widgets/error_state_widget.dart';
import 'package:the_hive_app/widgets/loading_state_widget.dart';
import 'package:timeago/timeago.dart' as timeago;

class PostVotersScreen extends StatefulWidget {
  const PostVotersScreen({
    Key? key,
    required this.author,
    required this.permlink,
    required this.details,
    required this.appData,
  }) : super(key: key);
  final String author;
  final String permlink;
  final SingleItemResponse details;
  final HiveBlocData appData;

  @override
  State<PostVotersScreen> createState() => _PostVotersScreenState();
}

class _PostVotersScreenState extends State<PostVotersScreen>
    with AutomaticKeepAliveClientMixin<PostVotersScreen> {
  @override
  bool get wantKeepAlive => true;
  late Future<PostVoters> loadData;

  @override
  void initState() {
    super.initState();
    loadData = _getPostVoters();
  }

  Future<PostVoters> _getPostVoters() async {
    var rpc = widget.appData.rpc;
    var request = Communicator().getRequestForUpVotesOfPost(
      rpc,
      widget.author,
      widget.permlink,
    );
    var response = await Communicator().getResponseString(request);
    return PostVoters.fromJsonString(response);
  }

  Widget _showListOfVoters(PostVoters data) {
    if (data.result.isEmpty) {
      return const Center(child: Text('No UpVotes so far 💔'));
    }
    return ListView.separated(
      itemBuilder: (c, i) {
        var item = data.result[i];
        var payout = widget.details.content.payout ?? 0.001;
        var nShares = widget.details.content.netRshares;
        var thisPayout = (item.rshares / nShares) * payout;
        var timeAgo =
        timeago.format(DateTime.tryParse(item.time ?? '') ?? DateTime.now());
        return ListTile(
          leading: CustomCircleAvatar(
            url: hiveBloc.userOwnerThumb(item.voter),
            width: 44,
            height: 44,
          ),
          title: Text(item.voter),
          subtitle: Text('${(item.percent / 100.0).toStringAsFixed(2)} % · $timeAgo'),
          trailing: Text('\$ ${thisPayout.toStringAsFixed(3)}'),
        );
      },
      separatorBuilder: (c, i) => const Divider(),
      itemCount: data.result.length,
    );
  }

  Widget _bodyFuture() {
    return FutureBuilder(
      future: loadData,
      builder: (builder, snapshot) {
        if (snapshot.hasError) {
          return ErrorStateWidget(onRetry: () {
            setState(() {
              loadData = _getPostVoters();
            });
          });
        } else if (snapshot.connectionState == ConnectionState.done) {
          var data = snapshot.data as PostVoters;
          return _showListOfVoters(data);
        } else {
          return const LoadingStateWidget();
        }
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return _bodyFuture();
  }
}
