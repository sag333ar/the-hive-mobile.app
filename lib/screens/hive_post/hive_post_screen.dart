import 'package:flutter/material.dart';
import 'package:the_hive_app/screens/hive_post/hive_post_comments.dart';
import 'package:the_hive_app/screens/hive_post/hive_post_details.dart';
import 'package:the_hive_app/screens/hive_post/hive_post_votes.dart';
import 'package:the_hive_app/utilities/hive_bloc.dart';
import 'package:the_hive_app/utilities/models/feed.dart';
import 'package:the_hive_app/utilities/request_generator.dart';
import 'package:the_hive_app/widgets/custom_circle_avatar.dart';
import 'package:the_hive_app/widgets/error_state_widget.dart';
import 'package:the_hive_app/widgets/loading_state_widget.dart';
import 'package:share_plus_dialog/share_plus_dialog.dart';

class HivePostScreen extends StatefulWidget {
  const HivePostScreen({
    Key? key,
    required this.author,
    required this.permlink,
    required this.appData,
  }) : super(key: key);
  final String author;
  final String permlink;
  final HiveBlocData appData;

  @override
  State<HivePostScreen> createState() => _HivePostScreenState();
}

class _HivePostScreenState extends State<HivePostScreen> {
  late int selectedTabIndex;
  late Future<SingleItemResponse> loadData;

  @override
  void initState() {
    super.initState();
    loadData = _getSingleItemResponse();
    selectedTabIndex = 0;
  }

  String _titleText() {
    return ["Post", "Votes", "Comments"][selectedTabIndex];
  }

  List<Tab> _tabs() {
    return [
      const Tab(icon: Icon(Icons.note_alt)),
      const Tab(icon: Icon(Icons.thumbs_up_down)),
      const Tab(icon: Icon(Icons.comment)),
    ];
  }

  List<Widget> _tabViews(SingleItemResponse data) {
    return [
      PostDetailsScreen(
        author: widget.author,
        permlink: widget.permlink,
        details: data,
        appData: widget.appData,
      ),
      PostVotersScreen(
        author: widget.author,
        permlink: widget.permlink,
        details: data,
        appData: widget.appData,
      ),
      HivePostCommentsTabScreen(
        author: widget.author,
        permlink: widget.permlink,
        details: data,
        appData: widget.appData,
      ),
    ];
  }

  Future<SingleItemResponse> _getSingleItemResponse() async {
    var author = widget.author.replaceAll("@", "");
    var rpc = widget.appData.rpc;
    var request = Communicator().getRequestForPost(
      rpc,
      author,
      widget.permlink,
    );
    var response = await Communicator().getResponseString(request);
    return SingleItemResponse.fromJsonStringForKey(
      forKey: "$author/${widget.permlink}",
      string: response,
    );
  }

  Widget _bodyFuture() {
    return FutureBuilder(
      future: loadData,
      builder: (builder, snapshot) {
        if (snapshot.hasError) {
          return _errorScaffold();
        } else if (snapshot.connectionState == ConnectionState.done) {
          var data = snapshot.data as SingleItemResponse;
          return _tabBar(data);
        } else {
          return _loadingScaffold();
        }
      },
    );
  }

  Widget _tabBar(SingleItemResponse data) {
    return DefaultTabController(
      length: _tabs().length,
      initialIndex: selectedTabIndex,
      child: Builder(
        builder: (context) {
          final tabController = DefaultTabController.of(context);
          tabController.addListener(() {
            setState(() {
              selectedTabIndex = tabController.index;
            });
          });
          return Scaffold(
            appBar: AppBar(
              centerTitle: false,
              title: ListTile(
                title: Text(data.content.title),
                subtitle: Text(_titleText()),
              ),
              actions: [
                IconButton(
                  onPressed: () {},
                  icon: CustomCircleAvatar(
                    url: hiveBloc.userOwnerThumb(widget.author),
                    width: 44,
                    height: 44,
                  ),
                ),
                IconButton(
                  icon: const Icon(Icons.share),
                  onPressed: () {
                    ShareDialog.share(
                        context,
                        'Hey! Check out this post https://the-hive-mobile.app/@${widget.author}/${widget.permlink}',
                        platforms:  SharePlatform.defaults,
                        isUrl: true
                    );
                  },
                ),
              ],
              bottom: TabBar(
                tabs: _tabs(),
              ),
            ),
            body: TabBarView(
              children: _tabViews(data),
            ),
          );
        },
      ),
    );
  }

  Widget _errorScaffold() {
    return Scaffold(
      appBar: AppBar(
        title: Text('@${widget.author}/${widget.permlink}'),
        actions: [
          IconButton(
            onPressed: () {},
            icon: CustomCircleAvatar(
              url: hiveBloc.userOwnerThumb(widget.author),
              width: 44,
              height: 44,
            ),
          ),
        ],
      ),
      body: ErrorStateWidget(onRetry: () {
        setState(() {
          loadData = _getSingleItemResponse();
        });
      }),
    );
  }

  Widget _loadingScaffold() {
    return Scaffold(
      appBar: AppBar(
        title: Text('@${widget.author}/${widget.permlink}'),
        actions: [
          IconButton(
            onPressed: () {},
            icon: CustomCircleAvatar(
              url: hiveBloc.userOwnerThumb(widget.author),
              width: 44,
              height: 44,
            ),
          ),
        ],
      ),
      body: const LoadingStateWidget(),
    );
  }

  @override
  Widget build(BuildContext context) {
    return _bodyFuture();
  }
}
