import 'package:flutter/material.dart';
import 'package:the_hive_app/utilities/hive_bloc.dart';
import 'package:the_hive_app/utilities/models/feed.dart';
import 'package:the_hive_app/utilities/models/post_comments.dart';
import 'package:the_hive_app/utilities/request_generator.dart';
import 'package:the_hive_app/widgets/error_state_widget.dart';
import 'package:the_hive_app/widgets/hive_comment_widget.dart';
import 'package:the_hive_app/widgets/loading_state_widget.dart';

class HivePostCommentsTabScreen extends StatefulWidget {
  const HivePostCommentsTabScreen({
    Key? key,
    required this.author,
    required this.permlink,
    required this.details,
    required this.appData,
  }) : super(key: key);
  final String author;
  final String permlink;
  final SingleItemResponse details;
  final HiveBlocData appData;

  @override
  State<HivePostCommentsTabScreen> createState() =>
      _HivePostCommentsTabScreenState();
}

class _HivePostCommentsTabScreenState extends State<HivePostCommentsTabScreen>
    with AutomaticKeepAliveClientMixin<HivePostCommentsTabScreen> {
  @override
  bool get wantKeepAlive => true;

  late Future<List<HiveComment>> _loadComments;

  @override
  void initState() {
    super.initState();
    _loadComments = _getPostComments(widget.author, widget.permlink);
  }

  Future<List<HiveComment>> _getPostComments(
    String author,
    String permlink,
  ) async {
    var rpc = widget.appData.rpc;
    var request = Communicator().getRequestForCommentsOfPost(
      rpc,
      widget.author,
      widget.permlink,
    );
    var response = await Communicator().getResponseString(request);
    var commentsResponse = hiveCommentsFromString(response, author, permlink);
    var comments = commentsResponse.result;
    for (var i = 0; i < comments.length; i++) {
      if (comments[i].children > 0) {
        if (comments
            .where((e) => e.parentPermlink == comments[i].permlink)
            .isEmpty) {
          var newComments =
              await _getPostComments(comments[i].author, comments[i].permlink);
          comments.insertAll(i + 1, newComments);
        }
      }
    }
    return comments;
  }

  Widget commentsListView(List<HiveComment> data) {
    return Container(
      margin: const EdgeInsets.only(top: 10, bottom: 10),
      child: ListView.separated(
        itemBuilder: (context, index) {
          return HiveCommentWidget(comment: data[index]);
        },
        separatorBuilder: (context, index) => const Divider(
          height: 10,
          color: Colors.blueGrey,
        ),
        itemCount: data.length,
      ),
    );
  }

  Widget _bodyFuture() {
    return FutureBuilder(
      future: _loadComments,
      builder: (builder, snapshot) {
        if (snapshot.hasError) {
          return ErrorStateWidget(onRetry: () {
            setState(() {
              _loadComments = _getPostComments(widget.author, widget.permlink);
            });
          });
        } else if (snapshot.connectionState == ConnectionState.done) {
          var data = snapshot.data as List<HiveComment>;
          return commentsListView(data);
        } else {
          return const LoadingStateWidget();
        }
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return _bodyFuture();
  }
}
