import 'package:flutter/material.dart';
import 'package:markdown_widget/markdown_widget.dart';
import 'package:the_hive_app/utilities/hive_bloc.dart';
import 'package:the_hive_app/utilities/models/feed.dart';
import 'package:the_hive_app/widgets/three_speak_player.dart';
import 'package:youtube_player_iframe/youtube_player_iframe.dart';

class PostDetailsScreen extends StatefulWidget {
  const PostDetailsScreen({
    Key? key,
    required this.author,
    required this.permlink,
    required this.details,
    required this.appData,
  }) : super(key: key);
  final String author;
  final String permlink;
  final SingleItemResponse details;
  final HiveBlocData appData;

  @override
  State<PostDetailsScreen> createState() => _PostDetailsScreenState();
}

class _PostDetailsScreenState extends State<PostDetailsScreen> {

  Widget _getMarkDownWidget(String newText) {
    return MarkdownWidget(
      data: newText,
      styleConfig: StyleConfig(
        markdownTheme: widget.appData.isDarkMode
            ? MarkdownTheme.darkTheme
            : MarkdownTheme.lightTheme,
        pConfig: PConfig(
          custom: (node) {
            if (node.tag == "youtube") {
              return YoutubePlayer(
                controller: YoutubePlayerController.fromVideoId(
                    videoId: node.attributes["id"]!),
                aspectRatio: 16 / 9,
              );
            } else if (node.tag == "threespeak") {
              return SizedBox(
                width: double.infinity,
                height: 320,
                child: ThreeSpeakPlayer(
                  author: node.attributes["author"]!,
                  permlink: node.attributes["permlink"]!,
                ),
              );
            }
            return Container(height: 1);
          },
        ),
      ),
    );
  }

  Widget _body() {
    var data = widget.details;
    if (data == null) {
      return const Center(child: CircularProgressIndicator());
    }
    var width = MediaQuery.of(context).size.width;
    var newText = data.content.body ?? "No content found";
    newText = newText.replaceAllMapped(
        RegExp(r'(https:\/\/www.youtube.com\/watch\?v=)(...........)'),
            (match) {
          if (match.group(2) != null) {
            return '<youtube id="${match.group(2)}"></youtube>';
          }
          return '';
        });
    newText = newText.replaceAllMapped(
        RegExp(r'(\[\!\[\]\(https:\/\/ipfs-3speak\.b-cdn\.net\/ipfs\/.+\)\])'),
            (match) {
          if (match.group(1) != null) {
            return '<threespeak author="${widget.author}" permlink="${widget.permlink}"></threespeak>\n';
          }
          return '';
        });
    if (width > 1000) {
      return Container(
        margin: const EdgeInsets.all(10),
        child: Row(
          children: [
            SizedBox(width: (width - 900) / 2.0),
            Container(
              constraints: const BoxConstraints(maxWidth: 900),
              child: _getMarkDownWidget(newText),
            ),
          ],
        ),
      );
    } else {
      return Container(
        margin: const EdgeInsets.all(10),
        child: _getMarkDownWidget(newText),
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return _body();
  }
}
