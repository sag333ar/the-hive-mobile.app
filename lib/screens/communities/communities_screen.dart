import 'package:flutter/material.dart';
import 'package:the_hive_app/screens/home_page.dart';
import 'package:the_hive_app/utilities/hive_bloc.dart';
import 'package:the_hive_app/utilities/models/communities.dart';
import 'package:the_hive_app/utilities/request_generator.dart';
import 'package:the_hive_app/widgets/custom_circle_avatar.dart';
import 'package:the_hive_app/widgets/error_state_widget.dart';
import 'package:the_hive_app/widgets/loading_state_widget.dart';

class CommunitiesScreen extends StatefulWidget {
  const CommunitiesScreen({
    Key? key,
    required this.appData,
  }) : super(key: key);
  final HiveBlocData appData;

  @override
  State<CommunitiesScreen> createState() => _CommunitiesScreenState();
}

class _CommunitiesScreenState extends State<CommunitiesScreen>
    with AutomaticKeepAliveClientMixin<CommunitiesScreen> {
  @override
  bool get wantKeepAlive => true;

  TextEditingController searchController = TextEditingController();

  late Future<void> loadCommunities;
  var isLoadingNextPage = false;
  List<CommunityItem> items = [];
  var isLoading = false;

  @override
  void initState() {
    super.initState();
    loadCommunities = getFeedData();
  }

  Future<void> getFeedData() async {
    setState(() {
      isLoading = true;
      items = [];
    });
    var request = Communicator().getRequestForCommunities(
      widget.appData.rpc,
      "rank",
      searchController.text,
    );
    var response = await Communicator().getResponseString(request);
    var responseData = CommunitiesResponseModel.fromJsonString(response);
    setState(() {
      items = responseData.result;
      isLoading = false;
    });
  }

  Widget _listView() {
    return ListView.separated(
      itemBuilder: (c, i) {
        var line2 =
            "${items[i].subscribers} Subscribers · ${items[i].numAuthors} Authors";
        var line3 =
            "${items[i].numPending} Posts · ${items[i].sumPending} Hive Pending Rewards";
        return ListTile(
          leading: CustomCircleAvatar(
            height: 44,
            width: 44,
            url: hiveBloc.userOwnerThumb(items[i].name),
          ),
          title: Text(items[i].title),
          subtitle: Text('${items[i].about}\n\n$line2\n$line3'),
          onTap: () {
            var screen = MyHomePage(
              feed: HomeFeedStyle.trending,
              tag: items[i].name,
              communityName: items[i].title,
            );
            var route = MaterialPageRoute(builder: (c) => screen);
            Navigator.of(context).push(route);
          },
        );
      },
      separatorBuilder: (c, i) => const Divider(
        color: Colors.grey,
        height: 1,
      ),
      itemCount: items.length,
    );
  }

  Widget _displayData() {
    if (items.isEmpty) {
      return const Center(
        child: Text('No data found.\nPlease update 🔍 search term.'),
      );
    }
    return _listView();
  }

  Widget _container() {
    return Stack(
      children: [
        Container(
          margin: const EdgeInsets.only(top: 80),
          child: _displayData(),
        ),
        Container(
          margin: const EdgeInsets.all(10),
          child: TextFormField(
            controller: searchController,
            decoration: InputDecoration(
              // icon: const Icon(Icons.search),
              label: const Text('Search'),
              hintText: 'Search community',
              suffixIcon: ElevatedButton(
                child: const Text('Search'),
                onPressed: () {
                  setState(() {
                    loadCommunities = getFeedData();
                  });
                },
              ),
            ),
            autocorrect: false,
            enabled: true,
          ),
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return SafeArea(
      child: FutureBuilder(
        future: loadCommunities,
        builder: (builder, snapshot) {
          if (snapshot.hasError) {
            return ErrorStateWidget(onRetry: () {
              setState(() {
                loadCommunities = getFeedData();
              });
            });
          } else if (snapshot.connectionState == ConnectionState.done) {
            return _container();
          } else {
            return const LoadingStateWidget();
          }
        },
      ),
    );
  }
}
