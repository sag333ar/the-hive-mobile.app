import 'package:flutter/material.dart';
import 'package:the_hive_app/utilities/hive_bloc.dart';
import 'package:the_hive_app/utilities/models/community_info.dart';
import 'package:the_hive_app/utilities/request_generator.dart';
import 'package:the_hive_app/widgets/custom_circle_avatar.dart';
import 'package:the_hive_app/widgets/loading_state_widget.dart';

class CommunityInfoTabScreen extends StatefulWidget {
  const CommunityInfoTabScreen({
    Key? key,
    required this.appData,
    required this.communityId,
  }) : super(key: key);
  final HiveBlocData appData;
  final String communityId;

  @override
  State<CommunityInfoTabScreen> createState() => _CommunityInfoTabScreenState();
}

class _CommunityInfoTabScreenState extends State<CommunityInfoTabScreen> {
  var isLoading = true;
  CommunityInfoResponse? response;

  @override
  void initState() {
    super.initState();
    getCommunityData();
  }

  Future<void> getCommunityData() async {
    setState(() {
      isLoading = true;
      response = null;
    });
    var request = Communicator().getRequestForCommunityDetails(
      widget.appData.rpc,
      widget.communityId,
    );
    var resp = await Communicator().getResponseString(request);
    var responseData = CommunityInfoResponse.fromJsonString(resp);
    setState(() {
      response = responseData;
      isLoading = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    if (isLoading) {
      return const LoadingStateWidget();
    } else if (response != null) {
      List<ListTile> team = response!.result.team.map((e) {
        return ListTile(
          leading: CustomCircleAvatar(
            height: 40,
            width: 40,
            url: hiveBloc.userOwnerThumb(e[0]),
          ),
          title: Text(e[0]),
          trailing: Text(e[1]),
        );
      }).toList();
      return Container(
        margin: const EdgeInsets.all(10),
        child: SingleChildScrollView(
          child: Column(
            children: [
                  CustomCircleAvatar(
                    height: 150,
                    width: 150,
                    url: hiveBloc.userOwnerThumb(widget.communityId),
                  ),
                  ListTile(
                    title: Text(
                      '@${response!.result.title}',
                      textAlign: TextAlign.center,
                    ),
                    subtitle: Text(
                      response!.result.about,
                      textAlign: TextAlign.center,
                    ),
                  ),
                  ListTile(
                    leading: const Icon(Icons.info, color: Colors.white),
                    subtitle: Text(response!.result.description),
                  ),
                  ListTile(
                    leading: const Icon(Icons.person_add, color: Colors.white),
                    title: const Text('Subscribers'),
                    trailing: Text('${response!.result.subscribers}'),
                  ),
                  ListTile(
                    leading:
                        const Icon(Icons.person_add_alt, color: Colors.white),
                    title: const Text('Authors'),
                    trailing: Text('${response!.result.numAuthors}'),
                  ),
                  ListTile(
                    leading:
                        const Icon(Icons.attach_money, color: Colors.white),
                    title: const Text('Pending Rewards'),
                    trailing: Text('\$ ${response!.result.sumPending}'),
                  ),
                  ListTile(
                    leading: const Icon(Icons.flag, color: Colors.white),
                    title: const Text('Important NOTE'),
                    subtitle: Text(response!.result.flagText),
                  ),
                ] +
                team,
          ),
        ),
      );
    } else {
      return const LoadingStateWidget();
    }
  }
}
