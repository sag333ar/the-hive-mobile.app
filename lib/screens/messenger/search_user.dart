import 'package:flutter/material.dart';
import 'package:the_hive_app/utilities/hive_bloc.dart';
import 'package:the_hive_app/utilities/models/messenger/search_user_response.dart';
import 'package:the_hive_app/utilities/request_generator.dart';
import 'package:the_hive_app/widgets/custom_circle_avatar.dart';
import 'package:the_hive_app/widgets/error_state_widget.dart';
import 'package:the_hive_app/widgets/loading_state_widget.dart';

class SearchUserScreen extends StatefulWidget {
  const SearchUserScreen({
    Key? key,
    required this.data,
    required this.onUserTap,
  }) : super(key: key);
  final HiveBlocData data;
  final Function onUserTap;

  @override
  State<SearchUserScreen> createState() => _SearchUserScreenState();
}

class _SearchUserScreenState extends State<SearchUserScreen> {
  TextEditingController searchController = TextEditingController();

  late Future<List<String>> loadUsers;
  var isLoading = false;

  @override
  void initState() {
    super.initState();
    loadUsers = getFeedData();
  }

  Future<List<String>> getFeedData() async {
    var request = Communicator().searchHiveUsers(
      widget.data.rpc,
      searchController.text,
    );
    var response = await Communicator().getResponseString(request);
    var responseData = SearchUserResponse.fromString(response);
    return responseData.result;
  }

  Widget _listView(List<String> items) {
    return ListView.separated(
      itemBuilder: (c, i) {
        return ListTile(
          leading: CustomCircleAvatar(
            height: 44,
            width: 44,
            url: hiveBloc.userOwnerThumb(items[i]),
          ),
          title: Text(items[i]),
          onTap: () {
            widget.onUserTap(items[i]);
          },
        );
      },
      separatorBuilder: (c, i) => const Divider(
        color: Colors.grey,
        height: 1,
      ),
      itemCount: items.length,
    );
  }

  Widget _displayData(List<String> items) {
    if (items.isEmpty) {
      return const Center(
        child: Text('No data found.\nPlease update 🔍 search term.'),
      );
    }
    return _listView(items);
  }

  Widget _container(List<String> data) {
    return Stack(
      children: [
        Container(
          margin: const EdgeInsets.only(top: 80),
          child: _displayData(data),
        ),
        Container(
          margin: const EdgeInsets.all(10),
          child: TextFormField(
            controller: searchController,
            decoration: InputDecoration(
              // icon: const Icon(Icons.search),
              label: const Text('Search'),
              hintText: 'Search community',
              suffixIcon: ElevatedButton(
                child: const Text('Search'),
                onPressed: () {
                  setState(() {
                    loadUsers = getFeedData();
                  });
                },
              ),
            ),
            autocorrect: false,
            enabled: true,
          ),
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Search User'),
      ),
      body: SafeArea(
        child: FutureBuilder(
          future: loadUsers,
          builder: (builder, snapshot) {
            if (snapshot.hasError) {
              return ErrorStateWidget(onRetry: () {
                setState(() {
                  loadUsers = getFeedData();
                });
              });
            } else if (snapshot.connectionState == ConnectionState.done) {
              var data = snapshot.data as List<String>;
              return _container(data);
            } else {
              return const LoadingStateWidget();
            }
          },
        ),
      ),
    );
  }
}
