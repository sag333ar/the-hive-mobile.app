import 'dart:developer';
import 'dart:js_util';

import 'package:flutter/material.dart';
import 'package:localstore/localstore.dart';
import 'package:the_hive_app/screens/messenger/messenger_details.dart';
import 'package:the_hive_app/screens/messenger/search_user.dart';
import 'package:the_hive_app/utilities/hive_bloc.dart';
import 'package:the_hive_app/utilities/models/account_history_for_transfers.dart';
import 'package:the_hive_app/utilities/pwa_comms.dart';
import 'package:the_hive_app/utilities/request_generator.dart';
import 'package:the_hive_app/widgets/custom_circle_avatar.dart';

class MessengerHome extends StatefulWidget {
  const MessengerHome({Key? key, required this.data, required this.user})
      : super(key: key);
  final HiveBlocData data;
  final String user;

  @override
  State<MessengerHome> createState() => _MessengerHomeState();
}

class _MessengerHomeState extends State<MessengerHome>
    with AutomaticKeepAliveClientMixin<MessengerHome> {
  @override
  bool get wantKeepAlive => true;
  List<TransferEntry> items = [];
  Map<String, List<TransferEntry>> dataMap = {};
  Map<String, List<TransferEntry>> filteredDataMap = {};
  var isLoading = false;
  var searchTerm = '';
  TextEditingController searchController = TextEditingController();

  List<TransferEntry> localItems = [];

  @override
  void initState() {
    super.initState();
    loadLocalMessages();
  }

  void loadLocalMessages() async {
    var entries = await TransferEntry.fromDB(widget.data.user?.name ?? widget.data.hiveKeychainUser ?? "");
    setState(() {
      localItems = entries;
      getFeedData(-1, 1);
    });
  }

  void processItems() async {
    Map<String, List<TransferEntry>> map = {};
    Map<String, List<TransferEntry>> newMap = {};
    var searchTerms = searchTerm.split(' ');
    for (var element in items) {
      var text = "";
      if (element.from == widget.user) {
        text = element.to;
      } else {
        text = element.from;
      }
      var array = map[text] ?? [];
      var newArray = newMap[text] ?? [];
      if (!(array.map((e) => e.trxId).contains(element.trxId))) {
        array.add(element);
      }
      for (var term in searchTerms) {
        if (text.toLowerCase().contains(term.toLowerCase()) ||
            element.memo.toLowerCase().contains(term.toLowerCase())) {
          if (!(newArray.map((e) => e.trxId).contains(element.trxId))) {
            newArray.add(element);
          }
        }
      }
      map[text] = array;
      if (newArray.isNotEmpty) {
        newMap[text] = newArray;
      }
    }
    setState(() {
      dataMap = map;
      if (searchTerm.isEmpty) {
        filteredDataMap = dataMap;
      } else {
        filteredDataMap = newMap;
      }
    });
  }

  Future<bool> shouldWeLoadNextPage() async {
    var itemsOpIds = items.map((e) => e.trxId).toList();
    var localItemsOpIds = localItems.map((e) => e.trxId).toList();
    var intersection = localItemsOpIds.toSet().intersection(itemsOpIds.toSet());
    List<TransferEntry> newLocalItems = localItems;
    List<TransferEntry> newItems = items;
    if (intersection.isNotEmpty) {
      for (var element in items) {
        if (!(localItemsOpIds.contains(element.trxId))) {
          newLocalItems.add(element);
          await TransferEntry.toDB(element, widget.data.user?.name ?? widget.data.hiveKeychainUser ?? "");
        }
      }
      for (var element in localItems) {
        if (!(itemsOpIds.contains(element.trxId))) {
          newItems.add(element);
        }
      }
      setState(() {
        items = newItems;
        localItems = newLocalItems;
        processItems();
      });
    } else {
      for (var element in items) {
        if (!(localItemsOpIds.contains(element.trxId))) {
          newLocalItems.add(element);
          await TransferEntry.toDB(element, widget.data.user?.name ?? widget.data.hiveKeychainUser ?? "");
        }
      }
    }
    return intersection.isEmpty;
  }

  Future<void> getFeedData(int start, int end) async {
    setState(() {
      isLoading = true;
    });
    log('start - $start, end $end');
    var rpc = widget.data.rpc;
    var request = Communicator().getRequestForTransferHistory(
      rpc,
      start,
      end,
      widget.user,
    );
    var response = await Communicator().getResponseString(request);
    var responseData = AccountHistoryForTransfers.fromJsonString(response);
    List<TransferEntry> messages = [];
    for (var element in responseData.items) {
      if (element.memo.startsWith("#")) {
        String? decoded;
        try {
          decoded = await promiseToFuture(getDecodedMemo(
            widget.data.user?.name ?? widget.data.hiveKeychainUser ?? '',
            element.memo,
          ));
        } catch (e) {
          log('Error decrypting data - $e');
        }
        if (decoded != null) {
          element.memo = decoded;
        }
      }
      messages.add(element);
    }
    setState(() {
      if (items.isEmpty) {
        items = messages;
      } else {
        items += messages;
      }
      items.sort((a, b) => b.timestamp.compareTo(a.timestamp));
      isLoading = false;
      processItems();
    });
    var shouldLoadNextPage = await shouldWeLoadNextPage();
    if (
    shouldLoadNextPage &&
        responseData.firstBlock != 0 &&
        (responseData.items.length == 1000 || start == -1)) {
      getFeedData(responseData.firstBlock, 1000);
    }
  }

  Widget _list() {
    if (filteredDataMap.keys.isEmpty) {
      return const Center(child: Text('No Data Found'));
    }
    var keys = filteredDataMap.keys.toList();
    return Stack(
      children: [
        Container(
          margin: const EdgeInsets.only(top: 120),
          child: ListView.separated(
            itemBuilder: (c, i) {
              var userThumb = hiveBloc.userOwnerThumb(keys[i]);
              var memo = filteredDataMap[keys[i]]?.first.memo ?? "";
              if (memo.length > 100) {
                memo = memo.substring(0, 99);
              }
              var amount =
                  filteredDataMap[keys[i]]?.first.amount ?? "0.000 Hive";
              if (memo.isEmpty) {
                memo = amount;
              } else {
                memo += "\n$amount";
              }
              return ListTile(
                leading:
                    CustomCircleAvatar(height: 50, width: 50, url: userThumb),
                title: Text(keys[i]),
                subtitle: Text("Last message: $memo"),
                trailing: Column(
                  children: [
                    Text("${filteredDataMap[keys[i]]?.length ?? 0}"),
                    const Icon(Icons.message, color: Colors.white),
                  ],
                ),
                onTap: () {
                  var data = filteredDataMap[keys[i]] ?? [];
                  var screen = MessengerDetailsScreen(
                    user: keys[i],
                    entries: data,
                    data: widget.data,
                    onAddMessage: (value) {
                      setState(() {
                        items.add(value);
                        items
                            .sort((a, b) => b.timestamp.compareTo(a.timestamp));
                        processItems();
                      });
                    },
                  );
                  var route = MaterialPageRoute(builder: (c) => screen);
                  Navigator.of(context).push(route);
                },
              );
            },
            separatorBuilder: (c, i) => const Divider(),
            itemCount: keys.length,
          ),
        ),
        Container(
          margin:
              const EdgeInsets.only(left: 10, right: 10, top: 30, bottom: 10),
          child: TextFormField(
            controller: searchController,
            decoration: InputDecoration(
              label: const Text('Search'),
              hintText: 'Search community',
              suffixIcon: ElevatedButton(
                child: const Text('Search'),
                onPressed: () {
                  setState(() {
                    searchTerm = searchController.text;
                    processItems();
                  });
                },
              ),
            ),
            autocorrect: false,
            enabled: true,
          ),
        ),
        Container(
          margin:
              const EdgeInsets.only(left: 10, right: 10, top: 10, bottom: 10),
          child: Row(
            children: [
              const Spacer(),
              isLoading
                  ? Column(
                      children: const [
                        Text("Loading Messages"),
                        SizedBox(height: 5),
                        CircularProgressIndicator(),
                      ],
                    )
                  : searchTerm.isEmpty
                      ? const Text("Showing All Messages")
                      : const Text("Filtered Messages"),
              const Spacer(),
            ],
          ),
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return Scaffold(
      appBar: AppBar(
        title: const Text('Messenger'),
        actions: [
          IconButton(
            onPressed: () {
              setState(() {
                items = [];
                getFeedData(-1, 1);
              });
            },
            icon: const Icon(Icons.refresh),
          ),
          IconButton(
            onPressed: () {
              var screen = SearchUserScreen(
                // dataMap: dataMap,
                data: widget.data,
                onUserTap: (value) {
                  var keys = dataMap.keys.toList();
                  var data = dataMap[value] ?? [];
                  var screen = MessengerDetailsScreen(
                    user: value,
                    entries: data,
                    data: widget.data,
                    onAddMessage: (value) {
                      setState(() {
                        items.add(value);
                        items
                            .sort((a, b) => b.timestamp.compareTo(a.timestamp));
                        processItems();
                      });
                    },
                  );
                  var route = MaterialPageRoute(builder: (c) => screen);
                  Navigator.of(context).pop();
                  Navigator.of(context).push(route);
                },
              );
              var route = MaterialPageRoute(builder: (c) => screen);
              Navigator.of(context).push(route);
            },
            icon: const Icon(Icons.edit_note),
          ),
        ],
      ),
      body: _list(),
    );
  }
}
