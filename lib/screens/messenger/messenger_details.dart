import 'dart:developer';
import 'dart:js_util';

import 'package:flutter/material.dart';
import 'package:flutter_chat_bubble/chat_bubble.dart';
import 'package:the_hive_app/utilities/hive_bloc.dart';
import 'package:the_hive_app/utilities/models/account_history_for_transfers.dart';
import 'package:the_hive_app/utilities/models/hive_message_response.dart';
import 'package:the_hive_app/utilities/pwa_comms.dart';
import 'package:the_hive_app/widgets/custom_circle_avatar.dart';
import 'package:timeago/timeago.dart' as timeago;

class MessengerDetailsScreen extends StatefulWidget {
  const MessengerDetailsScreen({
    Key? key,
    required this.entries,
    required this.user,
    required this.data,
    required this.onAddMessage,
  }) : super(key: key);
  final List<TransferEntry> entries;
  final String user;
  final HiveBlocData data;
  final Function onAddMessage;

  @override
  State<MessengerDetailsScreen> createState() => _MessengerDetailsScreenState();
}

class _MessengerDetailsScreenState extends State<MessengerDetailsScreen> {
  var sendingMessage = false;
  var messageText = '';
  late List<TransferEntry> entries;
  var isHiveTransfer = true;

  @override
  void initState() {
    super.initState();
    entries = widget.entries;
  }

  void showError(String string) {
    var snackBar = SnackBar(content: Text('Error: $string'));
    ScaffoldMessenger.of(context).showSnackBar(snackBar);
  }

  void showSuccess(String string) {
    var snackBar = SnackBar(
      content: Text(string),
      duration: const Duration(milliseconds: 1500),
    );
    ScaffoldMessenger.of(context).showSnackBar(snackBar);
  }

  Widget _listView() {
    return Container(
      margin: const EdgeInsets.only(top: 10, bottom: 130),
      child: ListView.separated(
        reverse: true,
        itemBuilder: (c, i) {
          var dateTime = DateTime.tryParse("${entries[i].timestamp}+00:00") ??
              DateTime.now();
          var timeAgo = timeago.format(dateTime);
          var username =
              widget.data.user?.name ?? widget.data.hiveKeychainUser ?? '';
          var memo = entries[i].memo;
          var amount = entries[i].amount;
          if (memo.isEmpty) {
            memo += "$amount\n\n$timeAgo";
          } else {
            memo += "\n\n$amount\n\n$timeAgo";
          }
          if (username == entries[i].from) {
            return ChatBubble(
              clipper: ChatBubbleClipper1(type: BubbleType.sendBubble),
              alignment: Alignment.topRight,
              margin: const EdgeInsets.only(top: 20),
              backGroundColor: Colors.green,
              child: Container(
                constraints: BoxConstraints(
                  maxWidth: MediaQuery.of(context).size.width * 0.7,
                ),
                child: Text(
                  memo,
                  style: const TextStyle(color: Colors.white),
                ),
              ),
            );
          } else {
            return ChatBubble(
              clipper: ChatBubbleClipper1(type: BubbleType.receiverBubble),
              backGroundColor: const Color(0xffE7E7ED),
              margin: const EdgeInsets.only(top: 20),
              child: Container(
                constraints: BoxConstraints(
                  maxWidth: MediaQuery.of(context).size.width * 0.7,
                ),
                child: Text(
                  memo,
                  style: const TextStyle(color: Colors.black),
                ),
              ),
            );
          }
        },
        separatorBuilder: (c, i) =>
            const Divider(height: 0, color: Colors.transparent),
        itemCount: entries.length,
      ),
    );
  }

  Widget _sendMessage() {
    var currency = isHiveTransfer ? "Hive" : "HBD";
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        const Spacer(),
        const Text("Start with # to send encrypted message"),
        Text("0.001 $currency transfer to ${widget.user}"),
        Row(children: [
          const Spacer(),
          const Text("Send 0.001 - HBD"),
          Switch(
            value: isHiveTransfer,
            onChanged: (newValue) {
              setState(() {
                isHiveTransfer = newValue;
              });
            },
          ),
          const Text('Hive'),
          const SizedBox(width: 5),
          const Spacer(),
        ]),
        Container(
          decoration: BoxDecoration(
            borderRadius: const BorderRadius.all(Radius.circular(10)),
            color: Theme.of(context).cardColor,
          ),
          child: sendingMessage
              ? const Center(child: CircularProgressIndicator())
              : Row(
                  children: [
                    const Spacer(),
                    SizedBox(
                      width: MediaQuery.of(context).size.width - 80,
                      child: TextField(
                        decoration: InputDecoration(
                          hintText: '0.001 $currency - Enter memo here.',
                          labelText: 'Message',
                          labelStyle: TextStyle(color: hiveBloc.themeColor),
                          focusedBorder: UnderlineInputBorder(
                            borderSide: BorderSide(
                                color: hiveBloc.themeColor, width: 1.0),
                          ),
                          border: UnderlineInputBorder(
                            borderSide: BorderSide(
                                color: hiveBloc.themeColor, width: 1.0),
                          ),
                        ),
                        onChanged: (newText) {
                          setState(() {
                            messageText = newText;
                          });
                        },
                      ),
                    ),
                    ElevatedButton(
                      onPressed: () async {
                        if (messageText.isEmpty) return;
                        setState(() {
                          sendingMessage = true;
                        });
                        var textToSend = messageText;
                        final String result = await promiseToFuture(sendMessage(
                          widget.data.user?.name ??
                              widget.data.hiveKeychainUser ??
                              '',
                          widget.user,
                          textToSend,
                          isHiveTransfer,
                        ));
                        HiveMessageResponse data =
                            HiveMessageResponse.fromString(result);
                        log('Data is - $data');
                        setState(() {
                          if (data.success && data.data != null && data.result != null) {
                            showSuccess(data.message);
                            var entry = TransferEntry(
                              to: data.data!.to,
                              from: data.data!.username,
                              memo: data.data!.memo,
                              amount:
                                  "${data.data!.amount} ${data.data!.currency}",
                              timestamp: DateTime.now().toIso8601String(),
                              trxId: data.result!.txId,
                            );
                            log('Entry is - $entry');
                            entries.insert(0, entry);
                            widget.onAddMessage(entry);
                          } else {
                            showError(data.message);
                          }
                          messageText = '';
                          sendingMessage = false;
                        });
                      },
                      child: const Text('Send'),
                    ),
                    const SizedBox(width: 10),
                  ],
                ),
        )
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: ListTile(
          leading: CustomCircleAvatar(
            url: hiveBloc.userOwnerThumb(widget.user),
            width: 44,
            height: 44,
          ),
          title: Text(widget.user),
          subtitle: Text("${entries.length} messages"),
        ),
      ),
      body: Stack(
        children: [
          _listView(),
          _sendMessage(),
        ],
      ),
    );
  }
}
