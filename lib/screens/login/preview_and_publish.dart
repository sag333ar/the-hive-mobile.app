import 'dart:convert';
import 'dart:developer';
import 'dart:js_util';

import 'package:flutter/material.dart';
// import 'package:flutter_markdown/flutter_markdown.dart';
// import 'package:flutter_markdown/flutter_markdown.dart';
import 'package:markdown_widget/markdown_widget.dart';
import 'package:provider/provider.dart';
import 'package:the_hive_app/utilities/hive_bloc.dart';
import 'package:the_hive_app/utilities/models/safe_convert.dart';
import 'package:the_hive_app/utilities/pwa_comms.dart';
import 'package:the_hive_app/utilities/random_string.dart';
import 'package:the_hive_app/utilities/request_generator.dart';
import 'package:the_hive_app/widgets/three_speak_player.dart';
import 'package:web_socket_channel/web_socket_channel.dart';
import 'package:youtube_player_iframe/youtube_player_iframe.dart';

class PreviewAndPublishScreen extends StatefulWidget {
  const PreviewAndPublishScreen({
    Key? key,
    required this.title,
    required this.description,
    required this.tags,
    required this.parentAuthor,
    required this.parentPermlink,
    required this.appData,
    required this.onDone,
  }) : super(key: key);
  final List<String> tags;
  final String title;
  final String description;
  final String? parentAuthor;
  final String? parentPermlink;
  final Function onDone;
  final HiveBlocData appData;

  @override
  State<PreviewAndPublishScreen> createState() =>
      _PreviewAndPublishScreenState();
}

class _PreviewAndPublishScreenState extends State<PreviewAndPublishScreen> {
  late WebSocketChannel socket;
  var timeoutValue = 0;

  void showError(String string) {
    var snackBar = SnackBar(content: Text('Error: $string'));
    ScaffoldMessenger.of(context).showSnackBar(snackBar);
  }

  void showMessage(String string) {
    var snackBar = SnackBar(content: Text(string));
    ScaffoldMessenger.of(context).showSnackBar(snackBar);
  }

  @override
  void initState() {
    super.initState();
    socket = WebSocketChannel.connect(
      Uri.parse(Communicator.hiveAuthServer),
    );
    socket.stream.listen((message) {
      var map = json.decode(message) as Map<String, dynamic>;
      var cmd = asString(map, 'cmd');
      if (cmd.isNotEmpty) {
        switch (cmd) {
          case "connected":
            setState(() {
              timeoutValue = asInt(map, 'timeout');
            });
            break;
          case "auth_wait":
            showError('You are not logged in. Log out & log in again.');
            break;
          case "auth_ack":
            showError('You are not logged in. Log out & log in again.');
            break;
          case "auth_nack":
            showError('You are not logged in. Log out & log in again.');
            break;
          case "sign_wait":
            var uuid = asString(map, 'uuid');
            showMessage(
                'Transaction - $uuid is waiting for approval. Please go to HiveKeychain / HiveAuth app & approve transaction.');
            break;
          case "sign_ack":
            var uuid = asString(map, 'uuid');
            showMessage(
                '🎉 Congratulation 🎉\n You have published the post. You approved $uuid using HiveKeychain / HiveAuth app.');
            Navigator.of(context).pop();
            Navigator.of(context).pop();
            break;
          case "sign_nack":
            var uuid = asString(map, 'uuid');
            showError(
                'Transaction - $uuid was not approved within time limit. Please try again.');
            break;
          case "sign_err":
            var uuid = asString(map, 'uuid');
            showError(
                'Error while signing using HiveAuth - Transaction - $uuid failed. Please try again.');
            break;
          default:
            log('Default case here');
        }
      }
    });
  }

  Widget _getMarkDownWidget(String newText) {
    return MarkdownWidget(
      data: newText,
      styleConfig: StyleConfig(
        markdownTheme: widget.appData.isDarkMode
            ? MarkdownTheme.darkTheme
            : MarkdownTheme.lightTheme,
        pConfig: PConfig(
          custom: (node) {
            if (node.tag == "youtube") {
              return YoutubePlayer(
                controller: YoutubePlayerController.fromVideoId(
                    videoId: node.attributes["id"]!),
                aspectRatio: 16 / 9,
              );
            } else if (node.tag == "threespeak") {
              return SizedBox(
                width: double.infinity,
                height: 320,
                child: ThreeSpeakPlayer(
                  author: node.attributes["author"]!,
                  permlink: node.attributes["permlink"]!,
                ),
              );
            }
            return Container(height: 1);
          },
        ),
      ),
    );
  }

  Widget _body() {
    var data = widget.description;
    data = "# ${widget.title}\n\n$data";
    var width = MediaQuery.of(context).size.width;
    var newText = data;
    newText = newText.replaceAllMapped(
        RegExp(r'(https:\/\/www.youtube.com\/watch\?v=)(...........)'),
        (match) {
      if (match.group(2) != null) {
        return '<youtube id="${match.group(2)}"></youtube>';
      }
      return '';
    });
    if (width > 1000) {
      return Container(
        margin: const EdgeInsets.all(10),
        child: Row(
          children: [
            SizedBox(width: (width - 900) / 2.0),
            Container(
              constraints: const BoxConstraints(maxWidth: 900),
              child: _getMarkDownWidget(newText),
            ),
          ],
        ),
      );
    } else {
      return Container(
        margin: const EdgeInsets.all(10),
        child: _getMarkDownWidget(newText),
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Preview & Publish'),
      ),
      body: _body(),
      floatingActionButton: ElevatedButton(
        onPressed: () async {
          if (widget.appData.user == null ||
              widget.appData.user?.authKey == null ||
              widget.appData.user?.name == null ||
              widget.appData.user?.token == null) {
            return;
          }
          try {
            var title = base64.encode(utf8.encode(widget.title));
            var description = base64.encode(utf8.encode(widget.description));
            final String result = await promiseToFuture(getEncryptedData(
              widget.appData.user!.authKey,
              widget.appData.user!.name,
              widget.appData.user!.token,
              widget.parentAuthor ?? '',
              widget.parentPermlink ?? widget.tags[0],
              title,
              description,
              widget.tags.join(" "),
            ));
            log('message is $result');
            socket.sink.add(result);
          } catch (e) {
            showError('Something went wrong - ${e.toString()}');
          }
        },
        child: const Text('Publish'),
      ),
    );
  }
}
