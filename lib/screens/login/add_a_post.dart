import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:the_hive_app/screens/login/preview_and_publish.dart';
import 'package:the_hive_app/utilities/hive_bloc.dart';
import 'package:the_hive_app/widgets/fab_custom.dart';
import 'package:the_hive_app/widgets/fab_overlay.dart';

class AddAPostScreen extends StatefulWidget {
  const AddAPostScreen({
    Key? key,
    required this.parentAuthor,
    required this.parentPermlink,
    required this.onDone,
  }) : super(key: key);
  final String? parentAuthor;
  final String? parentPermlink;
  final Function onDone;

  @override
  State<AddAPostScreen> createState() => _AddAPostScreenState();
}

class _AddAPostScreenState extends State<AddAPostScreen> {
  var title = '';
  var description = '';
  List<String> tags = [];
  var titleFieldController = TextEditingController();
  var tagsFieldController = TextEditingController();
  var bodyFieldController = TextEditingController();
  var isMenuOpen = false;

  Widget titleField() {
    return TextField(
      controller: titleFieldController,
      decoration: InputDecoration(
        hintText: 'Title goes here',
        labelText: 'Title',
        labelStyle: TextStyle(color: hiveBloc.themeColor),
        focusedBorder: UnderlineInputBorder(
          borderSide: BorderSide(color: hiveBloc.themeColor, width: 1.0),
        ),
        border: UnderlineInputBorder(
          borderSide: BorderSide(color: hiveBloc.themeColor, width: 1.0),
        ),
      ),
      onChanged: (text) {
        setState(() {
          title = text;
        });
      },
      maxLines: 1,
      minLines: 1,
      maxLength: 150,
    );
  }

  Widget _comment() {
    return TextFormField(
      controller: bodyFieldController,
      decoration: InputDecoration(
        hintText: widget.parentPermlink != null
            ? 'Add comment here'
            : 'Write your story',
        labelText: widget.parentPermlink != null ? 'Comment' : 'Body',
        labelStyle: TextStyle(color: hiveBloc.themeColor),
        focusedBorder: UnderlineInputBorder(
          borderSide: BorderSide(color: hiveBloc.themeColor, width: 1.0),
        ),
        border: UnderlineInputBorder(
          borderSide: BorderSide(color: hiveBloc.themeColor, width: 1.0),
        ),
      ),
      onChanged: (text) {
        setState(() {
          description = text;
        });
      },
      maxLines: 8,
      minLines: 5,
    );
  }

  Widget _tags() {
    return TextField(
      controller: tagsFieldController,
      decoration: InputDecoration(
        hintText: 'Space separated tags',
        labelText: 'Tags',
        labelStyle: TextStyle(color: hiveBloc.themeColor),
        focusedBorder: UnderlineInputBorder(
          borderSide: BorderSide(color: hiveBloc.themeColor, width: 1.0),
        ),
        border: UnderlineInputBorder(
          borderSide: BorderSide(color: hiveBloc.themeColor, width: 1.0),
        ),
      ),
      onChanged: (text) {
        setState(() {
          tags = text.trim().split(" ");
        });
      },
      maxLines: 1,
      minLines: 1,
    );
  }

  List<FabOverItemData> _fabItems(HiveBlocData appData) {
    var fabItems = [
      FabOverItemData(
        displayName: 'Clear all fields',
        icon: Icons.delete_sweep,
        onTap: () {
          setState(() {
            isMenuOpen = false;
            title = '';
            titleFieldController.clear();
            tagsFieldController.clear();
            bodyFieldController.clear();
            description = '';
            tags = [];
          });
        },
      ),
      FabOverItemData(
        displayName: 'Preview & Publish',
        icon: Icons.arrow_forward,
        onTap: () {
          setState(() {
            navigateToPreviewAndPublish(appData);
            isMenuOpen = false;
          });
        },
      ),
    ];
    fabItems.add(
      FabOverItemData(
        displayName: 'Close',
        icon: Icons.close,
        onTap: () {
          setState(() {
            isMenuOpen = false;
          });
        },
      ),
    );
    return fabItems;
  }

  Widget _fabContainer(HiveBlocData appData) {
    if (!isMenuOpen) {
      return FabCustom(
        icon: Icons.bolt,
        onTap: () {
          setState(() {
            isMenuOpen = true;
          });
        },
      );
    }
    return FabOverlay(
      items: _fabItems(appData),
      onBackgroundTap: () {
        setState(() {
          isMenuOpen = false;
        });
      },
    );
  }

  void navigateToPreviewAndPublish(HiveBlocData appData) {
    if (appData.user == null) {
      showError('You\'re not logged in');
      return;
    }
    if (widget.parentPermlink != null && description.isEmpty) {
      showError('Please add comment');
      return;
    }
    if (widget.parentPermlink == null && title.isEmpty) {
      showError('Please enter post title');
      return;
    }
    if (widget.parentPermlink == null && description.isEmpty) {
      showError('Please enter post body');
      return;
    }
    if (widget.parentPermlink == null && tags.isEmpty) {
      showError('Please enter at least one tag');
      return;
    }
    var screen = PreviewAndPublishScreen(
      tags: tags,
      title: title,
      description: description,
      parentAuthor: widget.parentAuthor,
      parentPermlink: widget.parentPermlink,
      appData: appData,
      onDone: widget.onDone,
    );
    var route = MaterialPageRoute(builder: (c) => screen);
    Navigator.of(context).push(route);
  }

  Widget _body(HiveBlocData appData) {
    return SafeArea(
      child: Stack(
        children: [
          Container(
            margin: const EdgeInsets.all(20),
            child: Column(
              children: widget.parentPermlink != null
                  ? [_comment()]
                  : [titleField(), _comment(), _tags()],
            ),
          ),
          _fabContainer(appData),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    var data = Provider.of<HiveBlocData>(context);
    return Scaffold(
      appBar: AppBar(
        title: Text(
            widget.parentPermlink != null ? 'Add a comment' : 'Add a Post'),
      ),
      body: _body(data),
    );
  }

  void showError(String string) {
    var snackBar = SnackBar(content: Text('Error: $string'));
    ScaffoldMessenger.of(context).showSnackBar(snackBar);
  }

  void showMessage(String string) {
    var snackBar = SnackBar(content: Text('Message: $string'));
    ScaffoldMessenger.of(context).showSnackBar(snackBar);
  }
}
