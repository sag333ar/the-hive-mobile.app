import 'dart:async';
import 'dart:convert';
import 'dart:developer';
import 'dart:js_util';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:qr_flutter/qr_flutter.dart';
import 'package:the_hive_app/utilities/hive_bloc.dart';
import 'package:the_hive_app/utilities/pwa_comms.dart';
import 'package:the_hive_app/utilities/request_generator.dart';
import 'package:web_socket_channel/web_socket_channel.dart';
import 'package:the_hive_app/utilities/models/safe_convert.dart';
import 'package:url_launcher/url_launcher.dart';

class HiveAuthBasedLoginScreen extends StatefulWidget {
  const HiveAuthBasedLoginScreen({
    Key? key,
    required this.data,
  }) : super(key: key);
  final HiveBlocData data;

  @override
  State<HiveAuthBasedLoginScreen> createState() =>
      _HiveAuthBasedLoginScreenState();
}

class _HiveAuthBasedLoginScreenState extends State<HiveAuthBasedLoginScreen>
    with TickerProviderStateMixin {
  var usernameController = TextEditingController();
  var postingKey = TextEditingController();
  late WebSocketChannel socket;
  String authKey = '';
  String username = '';
  String? qrCode;
  var timerDuration = 0;
  var timeoutValue = 0;
  var loadingQR = false;
  Timer? timer;
  var resultHasKeychainOnWindow = false;

  @override
  void initState() {
    super.initState();
    findHasKeychainOnWindow();
    socket = WebSocketChannel.connect(
      Uri.parse(Communicator.hiveAuthServer),
    );
    socket.stream.listen((message) {
      var map = json.decode(message) as Map<String, dynamic>;
      var cmd = asString(map, 'cmd');
      if (cmd.isNotEmpty) {
        switch (cmd) {
          case "connected":
            setState(() {
              timeoutValue = asInt(map, 'timeout');
            });
            break;
          case "auth_wait":
            var uuid = asString(map, 'uuid');
            var jsonData = {
              "account": usernameController.text,
              "uuid": uuid,
              "key": authKey,
              "host": Communicator.hiveAuthServer
            };
            var jsonString = json.encode(jsonData);
            var utf8Data = utf8.encode(jsonString);
            var qr = base64.encode(utf8Data);
            qr = "has://auth_req/$qr";
            setState(() {
              qrCode = qr;
              timerDuration = timeoutValue;
              timer = Timer.periodic(const Duration(seconds: 1), (timer) {
                if (timerDuration == 0) {
                  setState(() {
                    timer.cancel();
                    qrCode = null;
                  });
                } else {
                  setState(() {
                    timerDuration--;
                  });
                }
              });
              loadingQR = false;
            });
            break;
          case "auth_ack":
            var messageData = asString(map, 'data');
            decryptData(messageData);
            break;
          case "auth_nack":
            showError("Auth was not acknowledged");
            setState(() {
              qrCode = null;
              loadingQR = false;
            });
            break;
          default:
            log('Default case here');
        }
      }
    });
  }

  void findHasKeychainOnWindow() async {
    final bool result = await promiseToFuture(hasKeychainOnWindow());
    setState(() {
      resultHasKeychainOnWindow = result;
    });
  }

  void showError(String string) {
    var snackBar = SnackBar(content: Text('Error: $string'));
    ScaffoldMessenger.of(context).showSnackBar(snackBar);
  }

  void showMessage(String string) {
    var snackBar = SnackBar(content: Text(string));
    ScaffoldMessenger.of(context).showSnackBar(snackBar);
  }

  Widget _hiveUserName() {
    return TextField(
      decoration: InputDecoration(
        icon: Icon(Icons.alternate_email, color: hiveBloc.themeColor),
        label: const Text('Hive Username'),
        hintText: 'Enter Hive username here',
      ),
      autocorrect: false,
      controller: usernameController,
    );
  }

  Widget _hasButton() {
    return ElevatedButton(
      onPressed: () async {
        if (usernameController.text.isEmpty) {
          showMessage('Please enter Hive username');
          return;
        }
        final String result = await promiseToFuture(getRedirectUriData(
          usernameController.text,
        ));
        final Map<String, dynamic> data =
            json.decode(result) as Map<String, dynamic>;
        var dataForSocket = asString(data, 'data');
        var key = asString(data, 'authKey');
        var socketData = {
          "cmd": "auth_req",
          "account": usernameController.text,
          "data": dataForSocket,
        };
        var jsonEncodedData = json.encode(socketData);
        socket.sink.add(jsonEncodedData);
        setState(() {
          authKey = key;
          username = usernameController.text;
        });
      },
      child: const Text('Log in with HiveAuth'),
    );
  }

  Widget _hiveKeychainButton() {
    return ElevatedButton(
      style: ElevatedButton.styleFrom(backgroundColor: Colors.black),
      onPressed: () async {
        if (usernameController.text.isEmpty) {
          showMessage('Please enter Hive username');
          return;
        }
        final bool result = await promiseToFuture(hiveKeychainSign(
          usernameController.text,
        ));
        if (result) {
          showMessage('You\'ve logged in as - ${usernameController.text}');
          const storage = FlutterSecureStorage();
          await storage.write(
              key: 'hive_keychain_user', value: usernameController.text);
          await storage.delete(key: 'has_token');
          await storage.delete(key: 'has_expiry');
          await storage.delete(key: 'has_name');
          await storage.delete(key: 'has_auth_key');
          setState(() {
            hiveBloc.updateHKData(usernameController.text, widget.data);
            Navigator.of(context).pop();
          });
        }
      },
      child: Image.asset(
        'assets/hive-keychain-image.png',
        width: 220,
      ),
    );
  }

  Widget _showQRCodeAndKeychainButton(String qr) {
    return Center(
      child: Container(
        decoration: const BoxDecoration(color: Colors.white10),
        padding: const EdgeInsets.all(30),
        child: Column(
          children: [
            Image.asset('assets/hive_auth_button.png'),
            const SizedBox(height: 10),
            const Text('Scan QR Code'),
            const SizedBox(height: 10),
            InkWell(
              child: Container(
                decoration: const BoxDecoration(color: Colors.white),
                child: QrImage(
                  data: qr,
                  size: 200.0,
                  gapless: true,
                ),
              ),
              onTap: () {
                var url = Uri.parse(qr);
                launchUrl(url);
              },
            ),
            const SizedBox(height: 10),
            SizedBox(
              width: 200,
              child: LinearProgressIndicator(
                value: timerDuration.toDouble() / timeoutValue.toDouble(),
                semanticsLabel: 'Timeout Timer for HiveAuth QR',
                backgroundColor: Colors.white,
                color: hiveBloc.themeColor,
              ),
            ),
            const SizedBox(height: 10),
            const Text('- OR -'),
            const SizedBox(height: 10),
            const Text(
                'Tap on following "Keychain for Hive" button to authorize this request with "Keychain for Hive" app.'),
            const SizedBox(height: 20),
            ElevatedButton(
              onPressed: () {
                var url = Uri.parse(qr);
                launchUrl(url);
              },
              child: Image.asset('assets/hive-keychain-image.png'),
            ),
            const SizedBox(height: 20),
          ],
        ),
      ),
    );
  }

  Widget _loginForm() {
    return loadingQR
        ? const Center(child: CircularProgressIndicator())
        : qrCode == null
            ? Container(
                margin: const EdgeInsets.all(10),
                child: Column(
                  children: [
                    _hiveUserName(),
                    const SizedBox(height: 20),
                    !resultHasKeychainOnWindow
                        ? const Center(
                            child: Text(
                                'Please use HiveKeychain in-app-browser if you\'re on mobile.\n\nPlease install HiveKeychain Extension if you are on desktop.'),
                          )
                        : Column(
                            children: [
                              _hiveKeychainButton(),
                              const SizedBox(height: 20),
                            ],
                          ),
                    // _hasButton(),
                  ],
                ),
              )
            : _showQRCodeAndKeychainButton(qrCode!);
  }

  @override
  void dispose() {
    super.dispose();
    socket.sink.close();
  }

  void decryptData(String encryptedData) async {
    final String result = await promiseToFuture(getDecryptedHASToken(
      username,
      authKey,
      encryptedData,
    ));
    if (result.isNotEmpty &&
        result.contains(',') &&
        result.split(',').length == 2) {
      setState(() {
        timer?.cancel();
        qrCode = null;
      });
      showMessage('You\'ve logged in as - $username');
      const storage = FlutterSecureStorage();
      var token = result.split(',')[0];
      var expiry = result.split(',')[1];
      await storage.write(key: 'has_token', value: token);
      await storage.write(key: 'has_expiry', value: expiry);
      await storage.write(key: 'has_name', value: username);
      await storage.write(key: 'has_auth_key', value: authKey);
      await storage.delete(key: 'hive_keychain_user');
      setState(() {
        hiveBloc.updateUserData(
          HiveAuthData(
            token: result.split(',')[0],
            expiry: result.split(',')[0],
            name: username,
            authKey: authKey,
          ),
          widget.data,
        );
        Navigator.of(context).pop();
      });
    } else {
      showMessage(
          'Did not find token & expiry details from HiveAuth. Please go back & try again.');
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Hive Login'),
      ),
      body: _loginForm(),
    );
  }
}
