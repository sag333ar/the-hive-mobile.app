import 'dart:developer';

import 'package:adaptive_action_sheet/adaptive_action_sheet.dart';
import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:provider/provider.dart';
import 'package:the_hive_app/screens/communities/communities_screen.dart';
import 'package:the_hive_app/screens/communities/community_info_tab.dart';
import 'package:the_hive_app/screens/feeds/feeds_screen.dart';
import 'package:the_hive_app/screens/messenger/messenger_home.dart';
import 'package:the_hive_app/screens/threads/threads_container.dart';
import 'package:the_hive_app/utilities/hive_bloc.dart';
import 'package:the_hive_app/utilities/request_generator.dart';
import 'package:the_hive_app/widgets/custom_circle_avatar.dart';
import 'package:the_hive_app/widgets/fab_custom.dart';
import 'package:the_hive_app/widgets/fab_overlay.dart';

class MyHomePage extends StatefulWidget {
  const MyHomePage({
    super.key,
    required this.feed,
    required this.tag,
    required this.communityName,
  });

  final HomeFeedStyle feed;
  final String? tag;
  final String? communityName;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  late int selectedTabIndex;
  var isFilterMenuOn = false;

  @override
  void initState() {
    super.initState();
    selectedTabIndex = widget.feed.index;
  }

  String _titleText() {
    var communityArray = [
      '${widget.communityName} - Hot',
      '${widget.communityName} - Trending',
      '${widget.communityName} - New',
      '${widget.communityName} - Info'
    ];
    var regularArray = ['Hot', 'Trending', 'New', 'Communities'];
    return widget.communityName == null
        ? regularArray[selectedTabIndex]
        : communityArray[selectedTabIndex];
  }

  List<Tab> _tabs() {
    var noUserTabs = [
      const Tab(icon: Icon(Icons.local_fire_department_outlined)),
      const Tab(icon: Icon(Icons.trending_up)),
      const Tab(icon: Icon(Icons.pin)),
      // const Tab(icon: Icon(Icons.tag)),
      // const Tab(icon: Icon(Icons.settings)),
    ];
    if (widget.tag == null) {
      noUserTabs.add(const Tab(icon: Icon(Icons.people)));
    } else {
      noUserTabs.add(const Tab(icon: Icon(Icons.info)));
    }
    return noUserTabs;
    // var userTabs = [
    //   const Tab(icon: Icon(Icons.person_pin_sharp)),
    //   const Tab(icon: Icon(Icons.local_fire_department_outlined)),
    //   const Tab(icon: Icon(Icons.trending_up)),
    //   const Tab(icon: Icon(Icons.pin)),
    //   const Tab(icon: Icon(Icons.tag)),
    //   const Tab(icon: Icon(Icons.settings)),
    // ];
    // var tabs = user != null ? userTabs : noUserTabs;
    // return tabs;
  }

  List<Widget> _tabViews(HiveBlocData data) {
    var hotFeed = FeedsScreen(
      feedType: HomeFeedStyle.hot,
      appData: data,
      tag: widget.tag,
    );
    var trendingFeed = FeedsScreen(
      feedType: HomeFeedStyle.trending,
      appData: data,
      tag: widget.tag,
    );
    var newFeed = FeedsScreen(
      feedType: HomeFeedStyle.newFeed,
      appData: data,
      tag: widget.tag,
    );
    var communities = CommunitiesScreen(appData: data);
    return widget.tag == null
        ? [hotFeed, trendingFeed, newFeed, communities]
        : [
            hotFeed,
            trendingFeed,
            newFeed,
            CommunityInfoTabScreen(
              appData: data,
              communityId: widget.tag!,
            )
          ];
  }

  BottomSheetAction logoutOption(HiveBlocData appData) {
    return BottomSheetAction(
      title: const Text(
        'Log out',
        style: TextStyle(color: Colors.red),
      ),
      leading: const Icon(Icons.logout, color: Colors.red),
      onPressed: (context) async {
        const storage = FlutterSecureStorage();
        await storage.delete(key: 'has_token');
        await storage.delete(key: 'has_expiry');
        await storage.delete(key: 'has_name');
        await storage.delete(key: 'hk_name');
        await storage.delete(key: 'hk_key');
        await storage.delete(key: 'hive_keychain_user');
        setState(() {
          hiveBloc.updateUserData(null, appData);
          Navigator.of(context).pop();
        });
      },
    );
  }

  BottomSheetAction myChannel(HiveBlocData appData) {
    return BottomSheetAction(
      title: Text(
        'My Blog',
        style: TextStyle(color: hiveBloc.themeColor),
      ),
      leading: Icon(Icons.person, color: hiveBloc.themeColor),
      onPressed: (context) async {
        Navigator.of(context).pop();
        Navigator.of(context)
            .pushNamed('/@${appData.user?.name ?? appData.hiveKeychainUser}');
      },
    );
  }

  void logoutTapped(HiveBlocData appData) {
    showAdaptiveActionSheet(
      context: context,
      title: const Text('Select Option'),
      androidBorderRadius: 30,
      actions: [
        myChannel(appData),
        logoutOption(appData),
      ],
      cancelAction: CancelAction(
        title: Text('Cancel', style: TextStyle(color: hiveBloc.themeColor)),
      ),
    );
  }

  IconButton loginOrUserButton(HiveBlocData appData) {
    return appData.user == null && appData.hiveKeychainUser == null
        ? IconButton(
            onPressed: () {
              log('Hello');
              Navigator.of(context).pushNamed('/login');
            },
            icon: const Icon(Icons.login),
          )
        : IconButton(
            onPressed: () {
              logoutTapped(appData);
            },
            icon: CustomCircleAvatar(
              url: hiveBloc.userOwnerThumb(
                appData.user?.name ?? appData.hiveKeychainUser ?? '',
              ),
              width: 30,
              height: 30,
            ),
          );
  }

  AppBar _appBar(HiveBlocData appData) {
    return AppBar(
      centerTitle: false,
      title: Row(children: [
        Image.asset(
          'assets/Icon-maskable-512-white.png',
          width: 34,
          height: 34,
        ),
        const SizedBox(width: 5),
        Text(_titleText()),
      ]),
      actions: widget.tag == null
          ? [
              IconButton(
                onPressed: () {
                  hiveBloc.changeDarkMode(!appData.isDarkMode, appData);
                },
                icon: Icon(appData.isDarkMode
                    ? Icons.brightness_5
                    : Icons.brightness_2_outlined),
              ),
              loginOrUserButton(appData),
            ]
          : [],
      bottom: TabBar(
        tabs: _tabs(),
      ),
    );
  }

  List<FabOverItemData> _fabItems(HiveBlocData appData) {
    var isUser = appData.user != null || appData.hiveKeychainUser != null;
    return [
      isUser ? FabOverItemData(
        displayName: 'Messenger',
        icon: Icons.chat_bubble,
        onTap: () {
          setState(() {
            isFilterMenuOn = false;
            var screen = MessengerHome(data: appData, user: appData.user?.name ?? appData.hiveKeychainUser ?? 'mahdiyari');
            var route = MaterialPageRoute(builder: (c) => screen);
            Navigator.of(context).push(route);
          });
        },
      ) : null,
      // FabOverItemData(
      //   displayName: 'Threads',
      //   icon: Icons.forum,
      //   onTap: () {
      //     setState(() {
      //       isFilterMenuOn = false;
      //       const screen = ThreadsContainer();
      //       var route = MaterialPageRoute(builder: (c) => screen);
      //       Navigator.of(context).push(route);
      //     });
      //   },
      // ),
      isUser ? FabOverItemData(
        displayName: 'Write a Post',
        icon: Icons.note_alt,
        onTap: () {
          setState(() {
            isFilterMenuOn = false;
          });
        },
      ) : null,
      FabOverItemData(
        displayName: 'Close',
        icon: Icons.close,
        onTap: () {
          setState(() {
            isFilterMenuOn = false;
          });
        },
      ),
    ].whereType<FabOverItemData>().toList();
  }

  Widget _fabContainer(HiveBlocData appData) {
    if (!isFilterMenuOn) {
      return FabCustom(
        icon: Icons.bolt,
        onTap: () {
          setState(() {
            isFilterMenuOn = true;
          });
        },
      );
    }
    return FabOverlay(
      items: _fabItems(appData),
      onBackgroundTap: () {
        setState(() {
          isFilterMenuOn = false;
        });
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    var appData = Provider.of<HiveBlocData>(context);
    var count = _tabs().length;
    return DefaultTabController(
      length: count,
      initialIndex: selectedTabIndex,
      child: Builder(
        builder: (context) {
          final tabController = DefaultTabController.of(context);
          tabController.addListener(() {
            setState(() {
              selectedTabIndex = tabController.index;
            });
          });
          return Scaffold(
            appBar: _appBar(appData),
            body: Stack(
              children: [
                TabBarView(
                  children: _tabViews(appData),
                ),
                _fabContainer(appData)
              ],
            ),
          );
        },
      ),
    );
  }
}
