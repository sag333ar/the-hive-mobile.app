import 'dart:js_util';

import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:provider/provider.dart';
import 'package:the_hive_app/hive_app.dart';
import 'package:the_hive_app/utilities/hive_bloc.dart';
import 'package:the_hive_app/utilities/pwa_comms.dart';
import 'package:the_hive_app/widgets/loading_state_widget.dart';

import 'firebase_options.dart';

class MyApp extends StatefulWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  late Future<void> loadApp;
  static const storage = FlutterSecureStorage();
  static const defaultRpc = 'https://api.deathwing.me/';

  // 'https://api.hive.blog/';

  @override
  void initState() {
    super.initState();
    loadApp = initialiseFirebase();
  }

  Future<void> initialiseFirebase() async {
    // load .env
    await dotenv.load(fileName: ".env");
    // load firebase config
    await Firebase.initializeApp(
      options: DefaultFirebaseOptions.currentPlatform,
    );
    // load secure data
    String rpc = await storage.read(key: 'rpc') ?? defaultRpc;
    // ?? 'https://api.hive.blog/';
    String isDarkMode = await storage.read(key: 'is_dark_mode') ?? 'yes';
    String? hasToken = await storage.read(key: 'has_token');
    String? hasExpiry = await storage.read(key: 'has_expiry');
    String? hasName = await storage.read(key: 'has_name');
    String? hasAuthKey = await storage.read(key: 'has_auth_key');
    String? hkName = await storage.read(key: 'hk_name');
    String? hkKey = await storage.read(key: 'hk_key');
    String? hkUser = await storage.read(key: 'hive_keychain_user');
    final bool result = await promiseToFuture(hasKeychainOnWindow());
    if (hkName != null && hkKey != null) {
      hasToken = null;
      hasName = null;
      hasAuthKey = null;
      hasExpiry = null;
    }
    hiveBloc.updateAppData(
      HiveBlocData(
        isDarkMode: isDarkMode == 'yes',
        rpc: rpc,
        user: (hasToken != null &&
                hasExpiry != null &&
                hasName != null &&
                hasAuthKey != null)
            ? HiveAuthData(
                token: hasToken,
                expiry: hasExpiry,
                name: hasName,
                authKey: hasAuthKey,
              )
            : null,
        hiveKeychainUser: result && hkUser != null ? hkUser : null
      ),
    );
  }

  Widget _loading() {
    return MaterialApp(
      title: 'The-Hive-Mobile.App',
      theme: ThemeData.dark(),
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        appBar: AppBar(
          title: const Text('The-Hive-Mobile.App'),
        ),
        body: const LoadingStateWidget(),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    var initialData = HiveBlocData(
      isDarkMode: true,
      rpc: defaultRpc,
      user: null,
      hiveKeychainUser: null,
    );
    return StreamProvider<HiveBlocData>.value(
      value: hiveBloc.appData,
      initialData: initialData,
      child: FutureBuilder(
        future: loadApp,
        builder: (c, s) {
          if (s.connectionState == ConnectionState.done) {
            return const HiveApp();
          } else {
            return _loading();
          }
        },
      ),
    );
  }
}
